<form role="search" method="get" id="searchform" class="searchform" action="<?php echo get_site_url(); ?>">
                    <div class="form-group row">
                        <label class="screen-reader-text" for="s">
                                                    </label>
                        <div class="row" style="padding-left: 15px;padding-right: 15px;">
                        	Realizar una búsqueda:
                        	<div class="col-md-10">
                            <input style="width: 100%;border: 1px solid #70be4b;color: #7F703F; height: 40px; font-size: 15px;" type="text" value="" name="s" id="s">
                        </div>
                        <div class="col-md-2">
                            <button style="background: #49453C;color: white !important;border-radius: 14px;display: inline-block;padding: 5px;font-size: 1em;font-family: 'Verdana';font-weight: bold;font-style: normal;padding-left: 20px;padding-right: 20px;" type="submit" id="searchsubmit">
                            <i class="demo-icon icon-search"></i> <span>Buscar</span>
                        </button>
                        </div>
                        </div>
                        
                    </div>
                </form>
