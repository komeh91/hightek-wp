<?php get_header(); ?>
<div id="banner-area" >
			<img src="<?php bloginfo('template_url'); ?>/images/banner/banner1.jpg"  alt="" />
			<div class="parallax-overlay"></div>
			<!-- Subpage title start -->
			<div class="banner-title-content">
				<div class="text-center">
					<h2>Bolsa de trabajo</h2>
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb justify-content-center">
							<li class="breadcrumb-item"><a href="<?php echo get_site_url(); ?>">Inicio</a></li>
							<li class="breadcrumb-item text-white" aria-current="page">Vacantes</li>
						</ol>
					</nav>
				</div>
			</div><!-- Subpage title end -->
		</div><!-- Banner area end -->

		<!-- Main container start -->
		<section id="main-container">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<img src="<?php bloginfo('template_url'); ?>/images/pages/about-1.jpg" class=" border-r img-hightek" style="width: 70%;"s alt="">
						<!--
						<div class="gap-40"></div>
						<h3 class="title-border">Vacante 1</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo, natus voluptatibus
							adipisci porro magni
							dolore eos eius ducimus corporis quos perspiciatis quis iste, vitae autem libero
							ullam omnis cupiditate quam.
						</p>

						<div class="accordion" id="accordion2">
							<div class="card border rounded mb-2">
								<div class="card-header p-0">
									<a class="h4 collapsed mb-0 font-weight-bold text-uppercase d-block p-2 pl-5"
										data-toggle="collapse" data-target="#collapseA" aria-expanded="true"
										aria-controls="collapseA">Job Requirements</a>
								</div>
								<div id="collapseA" class="collapse" data-parent="#accordion2">
									<div class="card-body">
										<ul class="list-unstyled arrow">
											<li>B.Sc. from any reputed university</li>
											<li>MBA in Marketing from any reputed university is very much
												desired</li>
											<li>Two (02) years of experience in Software Company is an
												advantage.</li>
											<li>Experience as Business Analyst will be an added advantage</li>
											<li>Manage and execute lead generation activities.</li>
											<li>Help building marketing strategy, tool and approach by thorough
												research</li>
										</ul>
									</div>
								</div>
							</div>
 
							<div class="card border rounded mb-2">
								<div class="card-header p-0">
									<a class="h4 collapsed mb-0 font-weight-bold text-uppercase d-block p-2 pl-5"
										data-toggle="collapse" data-target="#collapseB" aria-expanded="true"
										aria-controls="collapseB">
										Job Responsibilities</a>
								</div>
								<div id="collapseB" class="collapse" data-parent="#accordion2">
									<div class="card-body">
										<ul class="list-unstyled arrow">
											<li>Global Business Development &amp; Strategic Account Management
											</li>
											<li>Marketing Research</li>
											<li>Marketing Collaterals development</li>
											<li>Business Analysis</li>
											<li>Manage and execute lead generation activities.</li>
											<li>Help building marketing strategy, tool and approach by thorough
												research</li>
										</ul>
									</div>
								</div>
							</div>
 

							<div class="card border rounded mb-2">
								<div class="card-header p-0">
									<a class="h4 collapsed mb-0 font-weight-bold text-uppercase d-block p-2 pl-5"
										data-toggle="collapse" data-target="#collapseC" aria-expanded="true"
										aria-controls="collapseC">
										What’s in it for you?</a>
								</div>
								<div id="collapseC" class="collapse" data-parent="#accordion2">
									<div class="card-body">
										<ul class="list-unstyled arrow">
											<li>Very Competitive Salary and Long Term Benefits with Excellent
												Career Opportunity in a Focused
												&amp; Stable organization.</li>
											<li>Training on new technology.</li>
											<li>Overseas Tour with Opportunity to work with Global Companies.
											</li>
											<li>Most importantly a friendly work environment with opportunity to
												learn from a number of highly
												skilled mentors.</li>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="gap-20"></div>
							<p><a href="#" class="btn btn-primary solid">Postularme <i
										class="fa fa-long-arrow-right"></i></a></p> 
						</div>
						  end -->
					</div>
					<!--/ Content col end -->

					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="sidebar sidebar-right">
							<!-- category start -->
							<div class="widget widget-categories">
								<h3>¿Qué buscamos?</h3>
								<p>Buscamos personas que amen la tecnología y busquen participar para hacerla mejor cada
									día.</p>
							</div><!-- category end -->

							<!-- tags start -->
							<div class="widget widget-tags">
								<h3>PRÓXIMAMENTE</h3>
								<p>Próximamente publicaremos las vacantes
									para que formes parte
									de nuestro equipo de trabajo.</p>
							</div><!-- tags end -->
						</div>
						<!--/ Sidebar end -->
					</div>
					<!--/ Sidebar col end -->
				</div>
				<!--/ row end -->
			</div>
			<!--/ container end -->
		</section>
		<!--/ Main container end -->

		<!-- Footer start -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12 footer-widget">
						<h3 class="widget-title">Proyectos Recientes</h3>
						<div class="latest-post-items media">
							<div class="latest-post-content media-body">
								<h4><a target="_blank" href="https://medicalfit.com.mx/">MedicalFit</a></h4>
								<p class="post-meta">
									<span class="author">El software que todos los nutriólogos aman.</span>
								</p>
							</div>
						</div>
						<div class="img-gallery">
							<div class="img-container">
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/1.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/1.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/2.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/2.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/3.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/3.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/4.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/4.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/5.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/5.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/6.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/7.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/8.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/8.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<!--/ End Recent Posts-->


					<div class="col-md-8 col-sm-12 footer-widget">
						<h3 style=" color: white; ">Datos de contacto:</h3>
						<div class="row"> 
							<div class="col-md-6">
								<h4>Email:</h4>
								<a href="mailto:ventas@hightek.com.mx" target="_top">ventas@hightek.com.mx</a>
							</div>
							<div class="col-md-6">
								<h4>Teléfonos</h4>
								<a href="tel:228290 1754" target="_top">+52 (228) 290 1754</a><br>
							    <a href="tel:228283 6940" target="_top">+52  (228) 283 6940</a><br>
 
							</div>
						</div>
						<div class="row">
							<div class="map col-md-8" id="map_canvas" data-latitude="" data-longitude=""
								data-marker="<?php bloginfo('template_url'); ?>/images/marker.png">
								<iframe
									src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d939.9997612532665!2d-96.93439361188314!3d19.541654563027674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db3201d8d80f3d%3A0xf96cea85b29f989f!2sHightek%20Desarrollo%20y%20Consultor%C3%ADa!5e0!3m2!1ses!2smx!4v1575509694125!5m2!1ses!2smx"
									width="550" height="400" frameborder="0" style="border:0;"
									allowfullscreen=""></iframe>
							</div>
							<!--<div class="col-md-4">
									<h4 style="margin-top: 30px;" > Dirección:</h4>
									<p>Fraternidad No.12 (3er Piso)
										<br> Esq. Av. Adolfo Ruiz Cortines
										<br> Col. Obrero Campesina,
										<br> 91020
										<br> Xalapa-Enríquez, Ver.</p>
								</div>-->
						</div>
					</div>
					<!--/ end flickr -->

				</div><!-- Row end -->
			</div><!-- Container end -->
		</footer><!-- Footer end -->


		<!-- Copyright start -->
		<section id="copyright" class="copyright angle">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="footer-social unstyled">
							<li>
								<a target="_blank" title="Twitter" href="https://twitter.com/hightekoficial">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
								</a>
								<a target="_blank" title="Facebook" href="https://www.facebook.com/hightekoficial/?ref=settings">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
								</a>
								<a target="_blank" title="linkedin"
									href="https://www.linkedin.com/company/hightek-desarrollo-y-consultoría/">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
								</a>
								<a target="_blank" title="Instagram" href="https://www.instagram.com/hightekxalapa/">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-instagram"></i></span>
								</a>
								<a target="_blank" href="https://api.whatsapp.com/send?phone=522282836940">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-whatsapp"></i></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!--/ Row end -->
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="copyright-info">
							&copy; Hightek<a href="<?php echo get_site_url(); ?>/terms"> Términos y condiciones</a></span>
						</div>
					</div>
				</div>
				<!--/ Row end -->
				<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
					<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
				</div>
			</div>
			<!--/ Container end -->
		</section>
		<!--/ Copyright end -->

<?php get_footer(); ?>
