<script type="text/javascript">
    $(document).ready(function(){
        $('.send-newletter').click(function(event){
			event.preventDefault();
			//validar email
			var valido = 0;
			var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
			var email = $("input[name='email']");
			var valor_email = email.val();
			if( valor_email == "" || valor_email ==" "){
				email.attr("placeholder", "Ingresar correo electrónico");
				valido++;
				email.val("");
			}
			else{
				if( !filter.test(valor_email) ){
					valido++;
					email.attr("placeholder", "Correo electrónico no válido");
					email.val("");
				}
			}
			if(valido == 0){
				var boton = $("button.send-newletter");
				var msg = $("h3.msg-newsletter");
				var action = $("input[name='action']");
				$("p.posts-loader").css('display','block');
				$.ajax({
					type : 'POST',
					url : "<?php echo get_bloginfo('url') ?>/wp-admin/admin-ajax.php",
	                data : {
	                	action  : 'send_newsletter',
	                    value : $('#form-lightbox-newsletter').serialize()
	                },
					dataType: "json",
					success : function( data, textStatus, XMLHttpRequest ) {
						console.log('data', data);
	                	//console.log('action', data['accion']);
	                	var status = data['type'];
	                	var accion = data['accion'];
	                	var text = data['text'];
	                	if( status == "success" ){
	                		//$("input[name='email']").prop("disabled", false);
	                		if( accion == "confirmar" ){
	                			//confirmar
	                			console.log('text', text);
	                			//ocultar email
	                			action.val("1");
	                			//mostrar confirmacion
	                			boton.text("Confirmar");
	                		}
	                		else{
	                			//confirmado
	                			boton.text("Suscribírse").prop("disabled", false);
	                			action.val("0");
	                		}
	                		//mostrart texto en verde
	                		msg.css('color','#14B64C');
	                	}
	                	else if(status == "error"){
	                		action.val("0");
	                		//mostrar texto en rojo
	                		msg.css('color','#FF0000');
	                	}
	                	msg.text(text);
	                	$("p.posts-loader").css('display','none');
	                }
				});
			}
		});
    });
</script>
<?php
	$sidebar_options = get_option("themeoption_sidebar_options");
	global $switched;
    switch_to_blog(1);
    	$banners_sidebar  = get_option("themeoption_banners_sidebar");
    restore_current_blog();
?>
<div id="sidebar-home" class="nota-item-g-right col-xs-12 col-sm-12 col-md-4 col-lg-4 <?php echo (!empty($sidebar_options['sidebar-active']) && $sidebar_options['sidebar-active'] == 'on') ? '' : 'hide'; ?>">
	<div class="wrapper-sidebar">
		<div class="sidebar-column-izq sin-padding col-xs-6 col-sm-6 col-md-12 col-lg-12 force-center-col">
			<div class="item-banner-atencion">
    			<a href="/atencion-ciudadana/">ATENCIÓN CIUDADANA</a>
    			<div class="clearfix"></div>
	    	</div>
			<div class="sin-padding">
				<div class="last-video"><?php get_post_category("videos", ""); ?></div>
			</div>
            <?php
            $count_file = count($banners_sidebar['image_file']);
            for($i=0; $i < $count_file; $i++){
            ?>
                <div class="banner-sidebar">
					<?php if(!empty($banners_sidebar['image_url'][$i])){ ?>
                    	<a href="<?php echo esc_url($banners_sidebar['image_url'][$i]); ?>" target="<?php echo ($banners_sidebar['new_window'][$i] == 1) ? '_blank' : '_self'; ?>">
                    		<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
                    	</a>
					<?php } else{ ?>
                    	<img class="img-full" src="<?php echo $banners_sidebar['image_file'][$i]; ?>">
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="lightbox-newsletter banner-sidebar newsletter boder-left-sidebar caja-sidebar" data-toggle="modal" data-target="#myModal">
	        	<h4>Suscríbete al <span>Newsletter</span></h4>
	        </div>
		</div><!--1er columna-->
		<div class="clearfix"></div>
	</div>
</div>
<!--Modal Newsletter-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id="modal-newsletter" class="modal-dialog modal-dialog-newsletter" role="document">
    <div class="modal-content modal-content-newsletter">
      <div class="modal-header modal-header-newsletter">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="body-footer-newsletter">
      	 <div class="modal-body modal-body-newsletter">
      	 	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      	 		<div class="col-iz-vernews col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
      	 		<div class="col-cent-vernews col-xs-4 col-sm-4 col-md-4 col-lg-4">
      	 			<img class="img-full" src="<?php echo get_bloginfo( 'template_url' ).'/images/footer/logotipo-footer.png'; ?>" border=0 alt="Newsletter Gobierno de Veracruz">
      	 		</div>
      	 		<div class="col-der-vernews col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
      	 	</div>
      	 	<div class="clearfix"></div>
	      	<h4>Newsletter Veracruz</h4>
	      	<h3 class="msg-newsletter">Suscríbete y recibe las noticias más destacadas de la semana vía e-mail</h3>
	    	<p class="posts-loader" style="display:none;opacity:1;margin-top:0px;margin-bottom:15px;">
	    		<img src="<?php bloginfo( 'template_url' ); ?>/images/loading.gif" />
	    	</p>
	    	<form id="form-lightbox-newsletter">
	        	<div class="col-md-12">
	            	<input type="email" name="email" placeholder="Escribe tu correo electrónico" required>
	            	<input type="hidden" name="no-spam"/>
	            	<input type="hidden" name="action" value="0" />
	            	<button type="submit" class="send-newletter">Suscribírse</button>
	        	</div>
	    	</form>
	      </div>
	      <div class="clearfix"></div>
	      <div class="modal-footer modal-footer-newsletter">
	      	<div class="ver-gob">
	      		<h3>VERACRUZ<span>.GOB.MX</span></h3>
	      	</div>
	      </div>
      </div><!--body-footer-newsletter-->
    </div><!--modal content-->
  </div><!--modal dialog-->
</div><!--modal-->
