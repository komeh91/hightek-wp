<?php get_header(); ?>
<div id="banner-area" style="height: 280px;">
			<img src="<?php bloginfo('template_url'); ?>/images/banner/banner1.jpg" style="height: 280px;" alt="" />
			<div class="parallax-overlay"></div>
			<!-- Subpage title start -->
			<div class="banner-title-content">
				<div class="text-center">
					<h2> Páginas WEB</h2>
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb justify-content-center">
							<li class="breadcrumb-item"><a href="<?php echo get_site_url(); ?>" style="color: white;">Inicio</a></li>
							<li class="breadcrumb-item text-white" aria-current="page">Cotización de planes página WEB</li>
						</ol>
					</nav>
				</div>
			</div><!-- Subpage title end -->
		</div><!-- Banner area end -->

		<!-- Pricing table start -->
		<section id="pricing" class="pricing">
			<div class="container">
				<div class="row">
					<div class="col-md-12 heading">
						<span class="title-icon float-left"><i class="fa fa-university"></i></span>
						<h2 class="title">Planes - página web </h2>
					</div>
				</div><!-- Title row end -->
				<div class="row">

					<!-- plan start -->
					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="1s">
						<div class="plan text-center featured">
							<span class="plan-name" style="background: aliceblue;">Paquete Básico<small>Anual</small>
							</span>
							<ul class="list-unstyled ">
								<li class="pad-price" style="padding-bottom: 9%!important;">Dirigido a estudiantes,
									profesionistas y emprendedores que buscan ofrecer sus
									servicios o para aquellas personas que quieren compartir algo con el mundo</li>
								<li>HOSPEDAJE<br><b>
										1 año</b></br></li>
								<li>DISPONIBILIDAD<br><b>
										24 horas / 7 días de la semana</b></br></li>
								<li class="pad-dom">DOMINIO<br><b>
										Personalizado (.com.mx)</b></br></li>
								<li>PÁGINAS<br><b>
										Hasta 3</b></br></li>
							</ul>
							<a class="btn btn-primary" href="<?php echo get_site_url(); ?>/contacto">Me interesa</a>
						</div>
					</div><!-- plan end -->

					<!-- plan start -->
					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="1.4s">
						<div class="plan text-center ">
							<span class="plan-name" style="background: aliceblue;">Paquete Negocio <small>Anual</small>
							</span>
							<ul class="list-unstyled">
								<li class="pad-price">Pensado para negocios que buscan darse a conocer y ofrecer sus
									servicios a la
									comunidad</li>
								<li>HOSPEDAJE<br><b> 1 año</b></br></li>
								<li>DISPONIBILIDAD<br><b> 24 horas / 7 días de la semana</b></br></li>
								<li class="pad-dom">DOMINIO<br><b> Personalizado (.com.mx)</b></br></li>
								<li>PÁGINAS<br><b> Hasta 6</b></br></li>
							</ul>
							<a class="btn btn-primary" href="<?php echo get_site_url(); ?>/contacto">Me interesa</a>
						</div>
					</div><!-- plan end -->

					<!-- plan start -->
					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="1.6s">
						<div class="plan text-center">
							<span class="plan-name" style="background: aliceblue;">Paquete
								Empresas<small>Anual</small></span>
							<ul class="list-unstyled">
								<li class="pad-price">Orientado a empresas que buscan impulsar su crecimiento adoptando
									tecnologías de la
									información</li>
								<li>HOSPEDAJE<br><b> 1 año</b></br></li>
								<li>DISPONIBILIDAD<br><b> 24 horas / 7 días de la semana</b></br></li>
								<li class="pad-dom">DOMINIO<br><b> Personalizado (.com) o (.com.mx)</b></br></li>
								<li>PÁGINAS<br><b> Hasta 15</b></br></li>
							</ul>
							<a class="btn btn-primary" href="<?php echo get_site_url(); ?>/contacto">Me interesa</a>
						</div>
					</div><!-- plan end -->
				</div>
				<!--/ Content row end -->
			</div>
			<!--/  Container end-->
		</section>
		<!--/ Pricing table end -->

		<section class="call-to-action">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3>¿Listo para impulsar y digitalizar tu negocio?</h3>
						<h3>Da click en el siguiente botón y comunícate con nosotros</h3>
						<a href="<?php echo get_site_url(); ?>/contacto" class="float-right btn btn-primary white">Comenzar</a>
					</div>
				</div>
			</div>
		</section>

		<!-- Footer start -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12 footer-widget">
						<h3 class="widget-title">Proyectos Recientes</h3>
						<div class="latest-post-items media">
							<div class="latest-post-content media-body">
								<h4><a target="_blank" href="https://medicalfit.com.mx/">MedicalFit</a></h4>
								<p class="post-meta">
									<span class="author">El software que todos los nutriólogos aman.</span>
								</p>
							</div>
						</div>
						<div class="img-gallery">
							<div class="img-container">
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/1.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/1.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/2.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/2.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/3.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/3.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/4.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/4.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/5.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/5.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/6.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/7.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/8.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/8.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<!--/ End Recent Posts-->


					<div class="col-md-8 col-sm-12 footer-widget">
						<div class="row"> 
							<div class="col-md-6">
								<h4>Email:</h4>
								<a href="mailto:ventas@hightek.com.mx" target="_top">ventas@hightek.com.mx</a>
							</div>
							<div class="col-md-6">
								<h4>Teléfonos</h4>
								<a href="tel:228290 1754" target="_top">+52 (228) 290 1754</a><br>
								<a href="tel:228283 6940" target="_top">+52  (228) 283 6940</a><br>
 
							</div>
						</div>
						<div class="row">
							<div class="map col-md-8" id="map_canvas" data-latitude="" data-longitude=""
								data-marker="<?php bloginfo('template_url'); ?>/images/marker.png">
								<iframe
									src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d939.9997612532665!2d-96.93439361188314!3d19.541654563027674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db3201d8d80f3d%3A0xf96cea85b29f989f!2sHightek%20Desarrollo%20y%20Consultor%C3%ADa!5e0!3m2!1ses!2smx!4v1575509694125!5m2!1ses!2smx"
									width="550" height="400" frameborder="0" style="border:0;"
									allowfullscreen=""></iframe>
							</div>
							<!--<div class="col-md-4">
									<h4 style="margin-top: 30px;" > Dirección:</h4>
									<p>Fraternidad No.12 (3er Piso)
										<br> Esq. Av. Adolfo Ruiz Cortines
										<br> Col. Obrero Campesina,
										<br> 91020
										<br> Xalapa-Enríquez, Ver.</p>
								</div>-->
						</div>
					</div>
				</div>
				<!--/ end flickr -->

			</div><!-- Row end -->
	</div><!-- Container end -->
	</footer><!-- Footer end -->


	<!-- Copyright start -->
	<section id="copyright" class="copyright angle">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<ul class="footer-social unstyled">
						<li>
							<a target="_blank" title="Twitter" href="https://twitter.com/hightekoficial">
								<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
							</a>
							<a target="_blank" title="Facebook"
								href="https://www.facebook.com/hightekoficial/?ref=settings">
								<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
							</a>
							<a target="_blank" title="linkedin"
								href="https://www.linkedin.com/company/hightek-desarrollo-y-consultoría/">
								<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
							</a>
							<a target="_blank" title="Instagram" href="https://www.instagram.com/hightekxalapa/">
								<span class="icon-pentagon wow bounceIn"><i class="fa fa-instagram"></i></span>
							</a>
							<a target="_blank" href="https://api.whatsapp.com/send?phone=522282836940">
								<span class="icon-pentagon wow bounceIn"><i class="fa fa-whatsapp"></i></span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!--/ Row end -->
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="copyright-info">
						&copy; Hightek<a href="<?php echo get_site_url(); ?>/terms"> Términos y condiciones</a></span>
					</div>
				</div>
			</div>
			<!--/ Row end -->
			<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
				<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
			</div>
		</div>
		<!--/ Container end -->
	</section>
	<!--/ Copyright end -->
<?php get_footer(); ?>
