<?php get_header(); ?>


		<!-- Slider start -->
		<section id="home" class="p-0">
			<div id="demo" class="carousel slide" data-ride="carousel">
				<ul class="carousel-indicators">
					<li data-target="#demo" data-slide-to="0" class="active"></li>
					<li data-target="#demo" data-slide-to="1"></li>
				</ul>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<div class="overlay2">
							<img src="<?php bloginfo('template_url'); ?>/images/slider/software.png" alt="slider">
						</div>
						<div class="carousel-caption">
							<h2 style="color: white;font-size: 3.2em;">Software personalizado</h2>
							<h3 style="color: white;font-size: 1.2em;">para impulsar su negocio empresarial</h3>
							<a href="<?php echo get_site_url(); ?>/contacto" class="btn btn-primary solid cd-btn"
								style="margin-bottom: 20%;">Contáctanos</a>
						</div>
					</div>

					<div class="carousel-item">
						<div class="overlay2">
							<img src="<?php bloginfo('template_url'); ?>/images/slider/aplicaciones.png">
						</div>
						<div class="carousel-caption">
							<h2 style="color: white;font-size: 2.2em;">Desarrollo de APPS móviles</h2>
							<h3 style="color: white;font-size: 1.2em;">Nos encargamos de crear el vínculo entre su
								empresa y sus clientes</h3>
							<a href="<?php echo get_site_url(); ?>/contacto" class="btn btn-primary solid cd-btn"
								style="margin-bottom: 20%;">Contáctanos</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ Slider end -->

		<!-- Portfolio start -->
		<section id="portfolio" class="portfolio">
			<div class="container">
				<div class="row">
					<div class="col-md-12 heading">
						<span class="title-icon classic float-left"><i class="fa fa-suitcase"></i></span>
						<h2 class="title classic">Experimenta las soluciones digitales <br>empresariales con Hightek
						</h2>
					</div>
				</div> <!-- Title row end -->

				<div class="row text-right">
					<div class="col-12">
						<div class="isotope-nav" data-isotope-nav="isotope">
							<ul>
								<li><a href="#" class="active" data-filter="*">Todo</a></li>
								<li><a href="#" data-filter=".development">Sistemas</a></li>
								<li><a href="#" data-filter=".apps">APPS</a></li>
								<li><a href="#" data-filter=".others">Otros</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid" data-placement="right">
				<div class="row isotope" id="isotope">
					<div class="col-sm-3 development isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/sistema.png" class="filter" alt="">
								<figcaption>
									<h3>Desarrollo de Software</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/software">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/sistema.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-sm-3 development isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/pagina.png" class="filter" alt="">
								<figcaption>
									<h3>Desarrollo de sitios web</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/sitios-web">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/pagina.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-sm-3 development isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/ecommerce.png" class="filter" alt="">
								<figcaption>
									<h3>Desarrollo de tiendas online</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/ecommerce">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/ecommerce.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-sm-3 apps isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/app.png" class="filter" alt="">
								<figcaption>
									<h3>Desarrollo de APPS Móviles</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/app">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/app.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-sm-3 others isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/consultoria.png" class="filter" alt="">
								<figcaption>
									<h3>Consultoría</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/consultoria">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/consultoria.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-sm-3 others isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/cursos.png" class="filter" alt="">
								<figcaption>
									<h3>Cursos y capacitación profesional</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/capacitacion">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/cursos.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-sm-3 others isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/portfolio7.png" class="filter" alt="">
								<figcaption>
									<h3>Redes Sociales</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/redes-sociales">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/portfolio7.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

				</div><!-- Content row end -->
			</div><!-- Container end -->
		</section><!-- Portfolio end -->

		<!-- Feature box start -->
		<section id="feature" class="feature">
			<div class="container">
				<div class="row">
					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-heart-o"></i></span>
						<div class="feature-content">
							<h3>WEB Responsive</h3>
							<p>Tu sitio se visualizará en cualquier dispositivo móvil o computadora.</p>
						</div>

					</div>
					<!--/ End first featurebox -->

					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-codepen"></i></span>
						<div class="feature-content">
							<h3>Dominio Personalizado</h3>
							<p>Proporcionamos un dominio que represente tu marca o negocio para que tu negocio obtenga
								una sólida presencia online.</p>
						</div>
					</div>
					<!--/ End 2nd featurebox -->

					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-film"></i></span>
						<div class="feature-content">
							<h3>Diseño limpio y moderno</h3>
							<p>Diseño páginas web profesionales para conseguir páginas web rápidas.</p>
						</div>

					</div>
					<!--/ End 3rd featurebox -->
				</div><!-- Content row end -->

				<div class="gap-40"></div>

				<div class="row">
					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-recycle"></i></span>
						<div class="feature-content">
							<h3>Análisis de costos</h3>
							<p>Para el desarrollo de software personalizado, enfocado a los requerimientos de su
								negocio.</p>
						</div>
					</div>
					<!--/ End first featurebox -->

					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-diamond"></i></span>
						<div class="feature-content">
							<h3>Software personalizado</h3>
							<p>Solución sólida y fácil de usar, desarrollada específicamente para las necesidades de su
								empresa.</p>
						</div>
					</div>


					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-pagelines"></i></span>
						<div class="feature-content">
							<h3>Procesos Empresariales Automatizados</h3>
							<p>Obtenga nuevos niveles de productividad y reduzca los costos y errores asociados con las
								tareas manuales.</p>
						</div>
					</div>
					<!--/ End 3rd featurebox -->

				</div><!-- Content row end -->

				<div class="gap-40"></div>

				<div class="row">
					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-newspaper-o"></i></span>
						<div class="feature-content">
							<h3>Aumento de la Eficiencia Empresarial</h3>
							<p>Mejore la toma de decisiones y competencia laboral, por medio del intercambio de informes
								en tiempo real.</p>
						</div>
					</div>
					<!--/ End 1st featurebox -->

					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-desktop"></i></span>
						<div class="feature-content">
							<h3>Elimine Procesos Innecesarios</h3>
							<p>El software empresarial lleva a la reducción de procesos redundantes y mejora el
								rendimiento de los empleados.</p>
						</div>
					</div>
					<!--/ End 2nd featurebox -->
					<!--/ End first featurebox -->

					<div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
						<span class="feature-icon float-left"><i class="fa fa-whatsapp"></i></span>
						<div class="feature-content">
							<h3>Soporte 24/7</h3>
							<p>Atención al cliente las 24 horas para obtener asistencia técnica en cualquier momento y
								en cualquier lugar.</p>
						</div>
					</div>
					<!--/ End first featurebox -->
				</div><!-- Content row end -->

			</div>
			<!--/ Container end -->
		</section>
		<!--/ Feature box end -->


		<section id="image-block" class="image-block p-0">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 ts-padding"
						style="height:650px;background:url(<?php bloginfo('template_url'); ?>/images/pages/contacto-02.png) 50% 50% / cover no-repeat;">
					</div>
					<div class="col-md-6 ts-padding img-block-right">
						<div class="img-block-head text-center">
							<h2>Conozca más sobre nuestra compañía</h2>
							<h3>¿Por qué elegirnos?</h3>
							<p>Principalmente, por que diseñamos y desarrollamos soluciones digitales que están
								centradas en los objetivos de su negocio empresarial, ayudándole a avanzar y obtener
								mayores ingresos.
							</p>
						</div>

						<div class="gap-30"></div>

						<div class="image-block-content">
							<span class="feature-icon float-left"><i class="fa fa-desktop"></i></span>
							<div class="feature-content">
								<h3>Personalización de software</h3>
								<p>Los integrantes de nuestro equipo están altamente calificados para la personalización
									del diseño, funcionalidad y estructura del sistema que tu negocio necesita.</p>
							</div>
						</div>
						<!--/ End 1st block -->

						<div class="image-block-content">
							<span class="feature-icon float-left"><i class="fa fa-diamond"></i></span>
							<div class="feature-content">
								<h3>Soluciones de vanguardia</h3>
								<p>Contamos con las últimas tecnologías digitales para hacer que tu negocio crezca con
									ayuda de la automatización de sus procesos.</p>
							</div>
						</div>
						<!--/ End 1st block -->

						<div class="image-block-content">
							<span class="feature-icon float-left"><i class="fa fa-street-view"></i></span>
							<div class="feature-content">
								<h3>Soporte las 24 horas</h3>
								<p>Y sobre todo, proporcionamos el servicio de soporte al cliente las 24 horas para
									obtener asistencia técnica en cualquier momento. </p>
							</div>
						</div>
						<!--/ End 1st block -->

					</div>
				</div>
			</div>
		</section>
		<!--/ Image block end -->

		<!-- Pricing table start -->
		<section id="pricing" class="pricing">
			<div class="container">
				<div class="row">
					<div class="col-md-12 heading">
						<span class="title-icon float-left"><i class="fa fa-university"></i></span>
						<h2 class="title">Planes - página web </h2>
					</div>
				</div><!-- Title row end -->
				<div class="row">

					<!-- plan start -->
					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="1s">
						<div class="plan text-center featured">
							<span class="plan-name" style="background: aliceblue;">Paquete Básico<small>Anual</small>
							</span>
							<ul class="list-unstyled ">
								<li class="pad-price" style="padding-bottom: 9%!important;">Dirigido a estudiantes,
									profesionistas y emprendedores que buscan ofrecer sus
									servicios o para aquellas personas que quieren compartir algo con el mundo</li>
								<li>HOSPEDAJE<br><b>
										1 año</b></br></li>
								<li>DISPONIBILIDAD<br><b>
										24 horas / 7 días de la semana</b></br></li>
								<li class="pad-dom">DOMINIO<br><b>
										Personalizado (.com.mx)</b></br></li>
								<li>PÁGINAS<br><b>
										Hasta 3</b></br></li>
							</ul>
							<a class="btn btn-primary" href="<?php echo get_site_url(); ?>/contacto">Me interesa</a>
						</div>
					</div><!-- plan end -->

					<!-- plan start -->
					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="1.4s">
						<div class="plan text-center ">
							<span class="plan-name" style="background: aliceblue;">Paquete Negocio <small>Anual</small>
							</span>
							<ul class="list-unstyled">
								<li class="pad-price">Pensado para negocios que buscan darse a conocer y ofrecer sus
									servicios a la
									comunidad</li>
								<li>HOSPEDAJE<br><b> 1 año</b></br></li>
								<li>DISPONIBILIDAD<br><b> 24 horas / 7 días de la semana</b></br></li>
								<li class="pad-dom">DOMINIO<br><b> Personalizado (.com.mx)</b></br></li>
								<li>PÁGINAS<br><b> Hasta 6</b></br></li>
							</ul>
							<a class="btn btn-primary" href="<?php echo get_site_url(); ?>/contacto">Me interesa</a>
						</div>
					</div><!-- plan end -->

					<!-- plan start -->
					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="1.6s">
						<div class="plan text-center">
							<span class="plan-name" style="background: aliceblue;">Paquete
								Empresas<small>Anual</small></span>
							<ul class="list-unstyled">
								<li class="pad-price">Orientado a empresas que buscan impulsar su crecimiento adoptando
									tecnologías de la
									información</li>
								<li>HOSPEDAJE<br><b> 1 año</b></br></li>
								<li>DISPONIBILIDAD<br><b> 24 horas / 7 días de la semana</b></br></li>
								<li class="pad-dom">DOMINIO<br><b> Personalizado (.com) o (.com.mx)</b></br></li>
								<li>PÁGINAS<br><b> Hasta 15</b></br></li>
							</ul>
							<a class="btn btn-primary" href="<?php echo get_site_url(); ?>/contacto">Me interesa</a>
						</div>
					</div><!-- plan end -->
				</div>
				<!--/ Content row end -->
			</div>
			<!--/  Container end-->
		</section>
		<!--/ Pricing table end -->

		<!-- Parallax 1 start -->
		<section class="parallax parallax1">
			<div class="parallax-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2>¿Listo para impulsar y digitalizar tu negocio?</h2>
						<h3>Da click en el siguiente botón y comunícate con nosotros</h3>
						<p style="text-align: center;">
							<a href="<?php echo get_site_url(); ?>/contacto" class="btn btn-primary solid">Impulsar mi negocio</a>
						</p>
					</div>
				</div>
			</div><!-- Container end -->
		</section><!-- Parallax 1 end -->

		<section class="testimonial feature">
			<div class="container">
				<div class="col-md-12 heading">
					<span class="title-icon float-left"><i class="fa fa-university"></i></span>
					<h2 class="title"> Conoce nuestros casos de éxito: </h2>
				</div>
			</div><!-- Title row end -->
			<div id="carouselExampleIndicators" class="carousel slide " data-ride="carousel">
				<ol class="carousel-indicators margin-bottom">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="8"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="9"></li>

				</ol>
				<div class="carousel-inner ">
					<div class="carousel-item active" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block height-img" style="width: 25% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/1.png"
								alt="First slide">
						</div>
						<div class="d-flex justify-content-center pt-3">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#modal1">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal1" tabindex="-1" role="dialog"
								aria-labelledby="modal1Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal1LongTitle">Detalles de servicios
												para GB Plus Intermercado</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											- Mantenimiento y desarrollo de sistema de administración.<br><br>
											- Mantenimiento y desarrollo de aplicaciones móviles
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block height-img" style="width: 14% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/2.png"
								alt="Second slide">
						</div>
						<div class="d-flex justify-content-center pt-3">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#exampleModalCenter">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
								aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLongTitle">Detalles de servicios
												para Atila</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											- Mantenimiento y desarrollo de sistema de información.
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block" style="width: 26% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/3.jpg"
								alt="Third slide">
						</div>
						<div class="d-flex justify-content-center ">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;
								margin-top:-8px" data-target="#modal3">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal3" tabindex="-1" role="dialog"
								aria-labelledby="modal3Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal3LongTitle">Detalles de servicios
												para ONBIT</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body"> <br>
											- Desarrollo de aplicaciones móviles
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block" style="width: 14% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/4.jpeg"
								alt="Fourth slide">
						</div>
						<div class="d-flex justify-content-center pt-3">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#modal4">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal4" tabindex="-1" role="dialog"
								aria-labelledby="modal4Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal4LongTitle">Detalles de servicios
												para Empeños Xalapa</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body"> <br>
											- Mantenimiento y desarrollo de sistema de administración.<br><br>
											- Mantenimiento y desarrollo de aplicaciones móviles
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block" style="width: 40% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/5.png"
								alt="Fith slide">
						</div>
						<div class="d-flex justify-content-center">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#modal5">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal5" tabindex="-1" role="dialog"
								aria-labelledby="modal5Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal5LongTitle">Servicios para The Hardware And
												Software Corp.</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body"> <br>
											- Mantenimiento y desarrollo de sistema de administración.<br>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block" style="width: 30% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/6.png"
								alt="Six slide">
						</div>
						<div class="d-flex justify-content-center pt-3">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#modal6">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal6" tabindex="-1" role="dialog"
								aria-labelledby="modal6Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal6LongTitle">Servicios para Harrison
												Nutrition</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body"> <br>
											- Mantenimiento y desarrollo de sistema de administración.<br><br>
											- Mantenimiento y desarrollo de aplicaciones móviles
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block" style="width: 30% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/7.png"
								alt="Seven slide">
						</div>
						<div class="d-flex justify-content-center pt-3">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#modal7">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal7" tabindex="-1" role="dialog"
								aria-labelledby="modal7Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal7LongTitle">Servicios para BENV POSGRADOS
											</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body"> <br>
											- Mantenimiento y desarrollo de sistema de administración.<br><br>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block" style="width: 20% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/8.png"
								alt="Eigth slide">
						</div>
						<div class="d-flex justify-content-center pt-3">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#modal8">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal8" tabindex="-1" role="dialog"
								aria-labelledby="modal8Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal8LongTitle">Servicios para NETLIFE</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body"> <br>
											- Mantenimiento y desarrollo de sistema de administración.<br><br>
											- Mantenimiento y desarrollo de aplicaciones móviles.

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block" style="width: 30% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/9.png"
								alt="Nine slide">
						</div>
						<div class="d-flex justify-content-center pt-3">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#modal9">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal9" tabindex="-1" role="dialog"
								aria-labelledby="modal9Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal9LongTitle">Servicios para VOIXTY</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body"> <br>
											- Mantenimiento y desarrollo de aplicaciones móviles iOS y Android.

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="carousel-item" style="height: 390px;">
						<div class="d-flex justify-content-center">
							<img class="d-block" style="width: 24% !important; height: 30% !important;" src="<?php bloginfo('template_url'); ?>/images/clientes/10.png"
								alt="Ten slide">
						</div>
						<div class="d-flex justify-content-center pt-3">
							<button type="button" class="btn btn-primary" data-toggle="modal" style="height: 35px;
								padding: 5px 20px;
								font-size: 12px;" data-target="#modal10">
								Más detalles
							</button>

							<!-- Modal -->
							<div class="modal fade" id="modal10" tabindex="-1" role="dialog"
								aria-labelledby="modal10Title" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="modal10LongTitle">Servicios para Medical Fit
											</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body"> <br>
											- Mantenimiento y desarrollo de sistema de administración.<br><br>

											- Mantenimiento y desarrollo de aplicaciones móviles iOS y Android.

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" style="padding: 5px 25px;"
												data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>

		</section>
		<!--/ Testimonial end-->


		<!-- Counter Strat -->
		<section class="ts_counter p-0">
			<div class="container-fluid">
				<div class="row facts-wrapper wow fadeInLeft text-center">
					<div class="facts one col-md-3 col-sm-6">
						<span class="facts-icon"><i class="fa fa-user"></i></span>
						<div class="facts-num">
							<span class="counter">20</span>
						</div>
						<h3>Clientes</h3>
					</div>

					<div class="facts two col-md-3 col-sm-6">
						<span class="facts-icon"><i class="fa fa-institution"></i></span>
						<div class="facts-num">
							<span class="counter">14</span>
						</div>
						<h3>Años de Experiencia</h3>
					</div>

					<div class="facts three col-md-3 col-sm-6">
						<span class="facts-icon"><i class="fa fa-suitcase"></i></span>
						<div class="facts-num">
							<span class="counter">25</span>
						</div>
						<h3>Proyectos</h3>
					</div>

					<div class="facts four col-md-3 col-sm-6">
						<span class="facts-icon"><i class="fa fa-trophy"></i></span>
						<div class="facts-num">
							<span class="counter">7500</span>
						</div>
						<h3>Horas de café</h3>
					</div>

				</div>
			</div>
			<!--/ Container end -->
		</section>
		<!--/ Counter end -->

		<!-- Service box start -->
		<section id="service" class="service ">
			<div class="container">
				<div class="row">
					<div class="col-md-12 heading">
						<span class="title-icon float-left"><i class="fa fa-cogs"></i></span>
						<h2>Nuestro
							equipo de desarrolladores se encuentran altamente calificados y utilizan las siguientes
							tecnologías:</h2>
					</div>
				</div><!-- Title row end -->
				<h3>Tecnologías web:</h3>

				<div class="row">
					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay=".5s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/JAVASCRIPT.png" style="width: 35%;" alt="">
							<h3>JavaScript</h3>
						</div>
					</div>
					<!--/ End first service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown d-none d-md-block" data-wow-delay=".8s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/CSS3.png" style="width: 25%; " alt="">
							<h3>CSS3</h3>
						</div>
					</div>
					<!--/ End Second service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="1.1s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/html.png" style="width: 35%;" alt="">
							<h3>HTML</h3>
						</div>
					</div>
					<!--/ End Third service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="1.4s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/php.png" style="width: 65%;" alt="">
							<h3>PHP</h3>
						</div>
					</div>
				</div><!-- Content row end -->

				<div class="row">
					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay=".5s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/oracleadf.png" style="width: 33%;" alt="">
							<h3>Oracle ADF</h3>
						</div>
					</div>
					<!--/ End first service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay=".8s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/microsoft.png" style="width: 63%;" alt="">
							<h3>Microsoft .NET</h3>
						</div>
					</div>
					<!--/ End Second service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="1.1s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/REST.png" style="width: 65%;" alt="">
							<h3>Servicios RESTFUL</h3>
						</div>
					</div>
					<!--/ End Third service -->
				</div><!-- Content row end -->
				<h3>Tecnologías de aplicaciones móviles:</h3>

				<div class="row">
					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay=".5s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/android.png" style="width: 33%;" alt="">
							<h3>Android</h3>
						</div>
					</div>
					<!--/ End first service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay=".8s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/swift.png" style="width: 53%;" alt="">
							<h3>IOS Swift</h3>
						</div>
					</div>
					<!--/ End Second service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="1.1s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/xamarin.png" style="width: 43%;" alt="">
							<h3>Xamarin</h3>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="1.1s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/kotlin.png" style="width: 77%;" alt="">
							<h3>Kotlin</h3>
						</div>
					</div>
					<!--/ End Third service -->
				</div><!-- Content row end -->
				<div class="row">
					<div class="col-md-12 heading">
						<h3>Tecnologías de bases de datos:</h3>
					</div>
				</div><!-- Title row end -->

				<div class="row">
					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay=".5s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/sql server.png" style="width: 40%;" alt="">
							<h3>SQL Server</h3>
						</div>
					</div>
					<!--/ End first service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay=".8s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/oracle.png" style="width: 33%;" alt="">
							<h3>Oracle Database</h3>
						</div>
					</div>
					<!--/ End Second service -->

					<div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay="1.1s">
						<div class="service-content text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/tecnologia/mysql.png" style="width: 63%;" alt="">
							<h3>MySQL</h3>
						</div>
					</div>
					<!--/ End Third service -->
				</div><!-- Content row end -->
			</div>
			<!--/ Container end -->
		</section>
		<!--/ Service box end -->

		<!-- Footer start -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12 footer-widget">
						<h3 class="widget-title">Proyectos Recientes</h3>
						<div class="latest-post-items media">
							<div class="latest-post-content media-body">
								<h4><a target="_blank" href="https://medicalfit.com.mx/">MedicalFit</a></h4>
								<p class="post-meta">
									<span class="author">El software que todos los nutriólogos aman.</span>
								</p>
							</div>
						</div>
						<div class="img-gallery">
							<div class="img-container">
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/1.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/1.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/2.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/2.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/3.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/3.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/4.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/4.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/5.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/5.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/6.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/7.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/8.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/8.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<!--/ End Recent Posts-->


					<div class="col-md-8 col-sm-12 footer-widget">
						<h3 style=" color: white; ">Datos de contacto:</h3>
						<div class="row">
							<div class="col-md-6">
								<h4>Email:</h4>
								<a href="mailto:ventas@hightek.com.mx" target="_top">ventas@hightek.com.mx</a>
							</div>
							<div class="col-md-6">
								<h4>Teléfonos</h4>
								<a  href="tel:228290 1754" target="_top">+52 (228) 290
									1754</a><br>
								<a  href="tel:228283 6940" target="_top">+52 (228) 283
									6940</a><br>

							</div>
						</div>
						<div class="row">
							<div class="map col-md-8" id="map_canvas" data-latitude="" data-longitude=""
								data-marker="<?php bloginfo('template_url'); ?>/images/marker.png">
								<!--<iframe width="100%" height="400" frameborder="0" style="border:0"
								src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCukY42svIdwAS_0_LAe3-08xTUZSi6qUY&amp;q=19.541724,-96.933863&amp;zoom=19"
								allowfullscreen=""></iframe>-->
								<iframe
									src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d939.9997612532665!2d-96.93439361188314!3d19.541654563027674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db3201d8d80f3d%3A0xf96cea85b29f989f!2sHightek%20Desarrollo%20y%20Consultor%C3%ADa!5e0!3m2!1ses!2smx!4v1575509694125!5m2!1ses!2smx"
									width="550" height="400" frameborder="0" style="border:0;"
									allowfullscreen=""></iframe>
							</div>
							<!--<div class="col-md-4">
								<h4 style="margin-top: 30px;" > Dirección:</h4>
								<p>Fraternidad No.12 (3er Piso)
									<br> Esq. Av. Adolfo Ruiz Cortines
									<br> Col. Obrero Campesina,
									<br> 91020
									<br> Xalapa-Enríquez, Ver.</p>
							</div>-->
						</div>
					</div>
					<!--/ end flickr -->

				</div><!-- Row end -->
			</div><!-- Container end -->
		</footer><!-- Footer end -->


		<!-- Copyright start -->
		<section id="copyright" class="copyright angle">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="footer-social unstyled">
							<li>
								<a target="_blank" title="Twitter" href="https://twitter.com/hightekoficial">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
								</a>
								<a target="_blank" title="Facebook"
									href="https://www.facebook.com/hightekoficial/?ref=settings">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
								</a>
								<a target="_blank" title="linkedin"
									href="https://www.linkedin.com/company/hightek-desarrollo-y-consultoría/">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
								</a>
								<a target="_blank" title="Instagram" href="https://www.instagram.com/hightekxalapa/">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-instagram"></i></span>
								</a>
								<a target="_blank" href="https://api.whatsapp.com/send?phone=522282836940">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-whatsapp"></i></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!--/ Row end -->
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="copyright-info">
							&copy; Hightek<a href="<?php echo get_site_url(); ?>/terms"> Términos y condiciones</a></span>
						</div>
					</div>
				</div>
				<!--/ Row end -->
				<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
					<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
				</div>
			</div>
			<!--/ Container end -->
		</section>
		<!--/ Copyright end -->

<?php get_footer(); ?>
