    </div>
    <!-- Tidio -->
    <script type='text/javascript' src='//code.tidio.co/y8oqhbxpo5ztnday8swbjxs3ztgzg3q8.js?ver=3.4.0'></script>
    <!-- jQuery -->
    <script src="<?php bloginfo('template_url'); ?>/plugins/jQuery/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="<?php bloginfo('template_url'); ?>/plugins/bootstrap/bootstrap.min.js"></script>
    <!-- Owl Carousel -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/owl/owl.carousel.js"></script>
    <!-- PrettyPhoto -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/jquery.prettyPhoto.js"></script>
    <!-- Bxslider -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/flex-slider/jquery.flexslider.js"></script>
    <!-- CD Hero slider -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/cd-hero/cd-hero.js"></script>
    <!-- Isotope -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/isotope.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/ini.isotope.js"></script>
    <!-- Wow Animation -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/wow.min.js"></script>
    <!-- Eeasing -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/jquery.easing.1.3.js"></script>
    <!-- Counter -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/jquery.counterup.min.js"></script>
    <!-- Waypoints -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/plugins/waypoints.min.js"></script>


    <!-- Main Script -->
    <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/popper.min.js"></script>


    <script src="//code.tidio.co/y8oqhbxpo5ztnday8swbjxs3ztgzg3q8.js"></script>
    <?php 
        if(is_page('contacto')){ ?>
            <script>

        //Funcion para mandar email
        (function () {
            emailjs.init("user_468Ewja7BZVfMTdzCF5tf");
        })();

        var myform = $("form#myform");
        myform.submit(function (event) {
            event.preventDefault();

            // Change to your service ID, or keep using the default service
            var service_id = "gmail";
            var template_id = "template_Z2uFtfPj";

            myform.find("button").text("Enviando...");
            emailjs.sendForm(service_id, template_id, myform[0])
                .then(function () {
                    alert("Mensaje enviado correctamente");
                    myform.find("button").text("Enviado...");
                    document.getElementById("myform").reset();
                    myform.find("button").text("Enviar Mensaje");
                }, function (err) {
                    alert("Send email failed!\r\n Response:\n " + JSON.stringify(err));
                    myform.find("button").text("Mensaje No Enviado");
                });
            return false;
        });
    </script>

    <?php    }
    ?>
</body>
</html>
