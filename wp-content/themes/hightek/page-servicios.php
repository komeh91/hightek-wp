<?php get_header(); ?>
<div id="banner-area" style="height: 300px;">
			<img src="<?php bloginfo('template_url'); ?>/images/banner/banner1.jpg" style="height: 300px;" alt="" />
			<div class="parallax-overlay"></div>
			<!-- Subpage title start -->
			<div class="banner-title-content">
				<div class="text-center">
					<h2>Soluciones empresariales</h2>
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb justify-content-center">
							<li class="breadcrumb-item"><a href="<?php echo get_site_url(); ?>/" style="color: white;">Inicio</a></li>
							<li class="breadcrumb-item text-white" aria-current="page">Servicios</li>
						</ol>
					</nav>
				</div>
			</div><!-- Subpage title end -->
		</div><!-- Banner area end -->

		<!-- Portfolio start -->
		<section id="portfolio" class="portfolio">
			<div class="container">
				<div class="row">
					<div class="col-md-12 heading">
						<span class="title-icon classic float-left"><i class="fa fa-suitcase"></i></span>
						<h2 class="title classic">Experimenta las soluciones digitales <br>empresariales con Hightek
						</h2>
					</div>
				</div> <!-- Title row end -->

				<div class="row text-right">
					<div class="col-12">
						<div class="isotope-nav" data-isotope-nav="isotope">
							<ul>
								<li><a href="#" class="active" data-filter="*">Todo</a></li>
								<li><a href="#" data-filter=".development">Sistemas</a></li>
								<li><a href="#" data-filter=".apps">APPS</a></li>
								<li><a href="#" data-filter=".others">Otros</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid">
				<div class="row isotope" id="isotope">
					<div class="col-md-3 development isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/sistema.png" class="filter" alt="">
								<figcaption>
									<h3>Desarrollo de Software</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/software">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/sistema.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-md-3 development isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/pagina.png" class="filter" alt="">
								<figcaption>
									<h3>Desarrollo de sitios web</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/sitios-web">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/pagina.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-md-3 development isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/ecommerce.png" class="filter" alt="">
								<figcaption>
									<h3>Desarrollo de tiendas online</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/ecommerce">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/ecommerce.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-md-3 apps isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/app.png" class="filter" alt="">
								<figcaption>
									<h3>Desarrollo de APPS Móviles</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/app">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/app.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-md-3 others isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/consultoria.png" class="filter" alt="">
								<figcaption>
									<h3>Consultoría</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/consultoria">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/consultoria.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-md-3 others isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/cursos.png" class="filter" alt="">
								<figcaption>
									<h3>Cursos y capacitación profesional</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/capacitacion">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/cursos.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

					<div class="col-md-3 others isotope-item">
						<div class="grid">
							<figure class="m-0 effect-oscar">
								<img src="<?php bloginfo('template_url'); ?>/images/portfolio/portfolio7.png" class="filter" alt="">
								<figcaption>
									<h3>Redes Sociales</h3>
									<a class="link mas-info" href="<?php echo get_site_url(); ?>/servicios/redes-sociales">Más información</a>
									<a class="view icon-pentagon" data-rel="prettyPhoto"
										href="<?php bloginfo('template_url'); ?>/images/portfolio/portfolio7.png"><i class="fa fa-search"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>

				</div><!-- Content row end -->
			</div><!-- Container end -->
		</section><!-- Portfolio end -->
		<div class="gap-40"></div>

		<!-- Footer start -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12 footer-widget">
						<h3 class="widget-title">Proyectos Recientes</h3>
						<div class="latest-post-items media">
							<div class="latest-post-content media-body">
								<h4><a target="_blank" href="https://medicalfit.com.mx/">MedicalFit</a></h4>
								<p class="post-meta">
									<span class="author">El software que todos los nutriólogos aman.</span>
								</p>
							</div>
						</div>
						<div class="img-gallery">
							<div class="img-container">
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/1.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/1.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/2.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/2.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/3.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/3.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/4.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/4.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/5.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/5.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/6.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/7.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/8.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/8.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<!--/ End Recent Posts-->


					<div class="col-md-8 col-sm-12 footer-widget">
						<div class="row">
							<div class="col-md-6">
								<h4>Email:</h4>
								<a href="mailto:ventas@hightek.com.mx" target="_top">ventas@hightek.com.mx</a>
							</div>
							<div class="col-md-6">
								<h4>Teléfonos</h4>
								<a href="tel:228290 1754" target="_top">+52 (228) 290 1754</a><br>
								<a href="tel:228283 6940" target="_top">+52  (228) 283 6940</a><br>
 
							</div>
						</div>
						
						<div class="row">
							<div class="map col-md-8" id="map_canvas" data-latitude="" data-longitude=""
								data-marker="<?php bloginfo('template_url'); ?>/images/marker.png">
								<!--<iframe width="100%" height="400" frameborder="0" style="border:0"
									src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCukY42svIdwAS_0_LAe3-08xTUZSi6qUY&amp;q=19.541724,-96.933863&amp;zoom=19"
									allowfullscreen=""></iframe>-->
								<iframe
									src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d939.9997612532665!2d-96.93439361188314!3d19.541654563027674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db3201d8d80f3d%3A0xf96cea85b29f989f!2sHightek%20Desarrollo%20y%20Consultor%C3%ADa!5e0!3m2!1ses!2smx!4v1575509694125!5m2!1ses!2smx"
									width="550" height="400" frameborder="0" style="border:0;"
									allowfullscreen=""></iframe>
							</div>
							<!--<div class="col-md-4">
									<h4 style="margin-top: 30px;" > Dirección:</h4>
									<p>Fraternidad No.12 (3er Piso)
										<br> Esq. Av. Adolfo Ruiz Cortines
										<br> Col. Obrero Campesina,
										<br> 91020
										<br> Xalapa-Enríquez, Ver.</p>
								</div>-->
						</div>
						<!--/ end flickr -->

					</div><!-- Row end -->
				</div><!-- Container end -->
		</footer><!-- Footer end -->


		<!-- Copyright start -->
		<section id="copyright" class="copyright angle">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="footer-social unstyled">
							<li>
								<a target="_blank" title="Twitter" href="https://twitter.com/hightekoficial">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
								</a>
								<a target="_blank" title="Facebook"
									href="https://www.facebook.com/hightekoficial/?ref=settings">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
								</a>
								<a target="_blank" title="linkedin"
									href="https://www.linkedin.com/company/hightek-desarrollo-y-consultoría/">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
								</a>
								<a target="_blank" title="Instagram" href="https://www.instagram.com/hightekxalapa/">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-instagram"></i></span>
								</a>
								<a target="_blank" href="https://api.whatsapp.com/send?phone=522282836940">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-whatsapp"></i></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!--/ Row end -->
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="copyright-info">
							&copy; Hightek<a href="<?php echo get_site_url(); ?>/terms"> Términos y condiciones</a></span>
						</div>
					</div>
				</div>
				<!--/ Row end -->
				<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
					<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
				</div>
			</div>
			<!--/ Container end -->
		</section>
		<!--/ Copyright end --s>
<?php get_footer(); ?>
