<?php get_header(); ?>

<div id="banner-area" >
			<img src="<?php bloginfo('template_url'); ?>/images/banner/banner1.jpg" alt="" />
			<div class="parallax-overlay"></div>
			<!-- Subpage title start -->
			<div class="banner-title-content">
				<div class="text-center">
					<h2>Sobre Hightek</h2>
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb justify-content-center">
							<li class="breadcrumb-item"><a href="<?php echo get_site_url(); ?>" style="color: white;">Inicio</a></li>
							<li class="breadcrumb-item text-white" aria-current="page">Nosotros</li>
						</ol>
					</nav>
				</div>
			</div><!-- Subpage title end -->
		</div><!-- Banner area end -->

		<!-- Main container start -->
		<section id="main-container">
			<div class="container">

				<!-- Company Profile -->
				<div class="row">
					<div class="col-md-12 heading">
						<span class="title-icon classic float-left"><i class="fa fa-suitcase"></i></span>
						<h2 class="title classic">Compañía</h2>
					</div>
				</div><!-- Title row end -->

				<div class="row landing-tab">
					<div class="col-md-3 col-sm-5">
						<div class="nav flex-column nav-pills border-right" id="v-pills-tab" role="tablist"
							aria-orientation="vertical">
							<a class="animated fadeIn nav-link py-4 active d-flex align-items-center" data-toggle="pill"
								href="#tab_1" role="tab" aria-selected="true">
								<i class="fa fa-info-circle mr-4"></i>
								<span class="h4 mb-0 font-weight-bold">¿Quiénes somos?</span>
							</a>
							<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill"
								href="#tab_2" role="tab" aria-selected="true">
								<i class="fa fa-briefcase mr-4"></i>
								<span class="h4 mb-0 font-weight-bold">Misión</span>
							</a>
							<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill"
								href="#tab_3" role="tab" aria-selected="true">
								<i class="fa fa-android mr-4"></i>
								<span class="h4 mb-0 font-weight-bold">Visión</span>
							</a>
						</div>
					</div>
					<div class="col-md-9 col-sm-7">
						<div class="tab-content" id="v-pills-tabContent">
							<div class="tab-pane pl-sm-5 fade show active animated fadeInLeft" id="tab_1"
								role="tabpanel">
								<img src="<?php bloginfo('template_url'); ?>/images/hightek.png" class="img-hightek widht-hightek" alt="">
								<h3>Somos una empresa 100% Xalapeña</h3>
								<p>Fundada en Xalapa, Veracruz por profesionales en tecnologías de la información
									orgullosamente egresados de la Universidad Veracruzana, nuestro objetivo es y será
									siempre, impulsar el desarrollo de nuestra comunidad ofreciendo soluciones que se
									adapten a sus necesidades.</p>
							</div>
							<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_2" role="tabpanel">
								<img src="<?php bloginfo('template_url'); ?>/images/mision.png" class="img-hightek" alt="">
								<h3>Misión</h3>
								<p>Facilitar los procesos empresariales de nuestros clientes a través de herramientas
									digitales a la medida de sus necesidades. </p>
							</div>
							<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_3" role="tabpanel">
								<img src="<?php bloginfo('template_url'); ?>/images/vision.png" class="img-hightek" alt="">
								<h3>Visión</h3>
								<p>Ser la empresa líder a nivel internacional en consultoría, producción y distribución
									de herramientas y soluciones en tecnologías de la información.</p>
							</div>
						</div>
					</div>
				</div>
				<!--/ Content row end -->
			</div>
			<!--/ 1st container end -->


			<div class="gap-60"></div>


			<!-- Counter Strat -->
			<div class="ts_counter_bg parallax parallax2">
				<div class="parallax-overlay"></div>
				<div class="container">
					<div class="row wow fadeInLeft text-center">
						<div class="facts col-md-3 col-sm-6">
							<span class="facts-icon"><i class="fa fa-user"></i></span>
							<div class="facts-num">
								<span class="counter">20</span>
							</div>
							<h3>Clientes</h3>
						</div>

						<div class="facts col-md-3 col-sm-6">
							<span class="facts-icon"><i class="fa fa-institution"></i></span>
							<div class="facts-num">
								<span class="counter">14</span>
							</div>
							<h3>Años de Experiencia</h3>
						</div>

						<div class="facts col-md-3 col-sm-6">
							<span class="facts-icon"><i class="fa fa-suitcase"></i></span>
							<div class="facts-num">
								<span class="counter">25</span>
							</div>
							<h3>Proyectos</h3>
						</div>

						<div class="facts col-md-3 col-sm-6">
							<span class="facts-icon"><i class="fa fa-trophy"></i></span>
							<div class="facts-num">
								<span class="counter">7500</span>
							</div>
							<h3>Horas de café</h3>
						</div>

						<div class="gap-40"></div>
					</div>
					<!--/ row end -->
				</div>
				<!--/ Container end -->
			</div>
			<!--/ Counter end -->

			<div class="gap-60"></div>
		</section>
		<!--/ Main container end -->

		<!-- Footer start -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-12 footer-widget">
						<h3 class="widget-title">Proyectos Recientes</h3>
						<div class="latest-post-items media">
							<div class="latest-post-content media-body">
								<h4><a target="_blank" href="https://medicalfit.com.mx/">MedicalFit</a></h4>
								<p class="post-meta">
									<span class="author">El software que todos los nutriólogos aman.</span>
								</p>
							</div>
						</div>
						<div class="img-gallery">
							<div class="img-container">
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/1.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/1.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/2.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/2.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/3.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/3.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/4.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/4.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/5.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/5.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/6.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/7.png" alt="">
								</a>
								<a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/8.png">
									<img src="<?php bloginfo('template_url'); ?>/images/gallery/8.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<!--/ End Recent Posts-->


					<div class="col-md-8 col-sm-12 footer-widget">
						<h3 style=" color: white; ">Datos de contacto:</h3>
						<div class="row">
							<div class="col-md-6">
								<h4>Email:</h4>
								<a href="mailto:ventas@hightek.com.mx" target="_top">ventas@hightek.com.mx</a>
							</div>
							<div class="col-md-6">
								<h4>Teléfonos</h4>
								<a href="tel:228290 1754" target="_top">+52 (228) 290 1754</a><br>
								<a href="tel:228283 6940" target="_top">+52 (228) 283 6940</a><br>
							</div>
						</div>
						<div class="row">
							<div class="map col-md-8" id="map_canvas" data-latitude="" data-longitude=""
								data-marker="<?php bloginfo('template_url'); ?>/images/marker.png">
								<iframe
									src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d939.9997612532665!2d-96.93439361188314!3d19.541654563027674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db3201d8d80f3d%3A0xf96cea85b29f989f!2sHightek%20Desarrollo%20y%20Consultor%C3%ADa!5e0!3m2!1ses!2smx!4v1575509694125!5m2!1ses!2smx"
									width="550" height="400" frameborder="0" style="border:0;"
									allowfullscreen=""></iframe>
							</div>
							<!--<div class="col-md-4">
									<h4 style="margin-top: 30px;" > Dirección:</h4>
									<p>Fraternidad No.12 (3er Piso)
										<br> Esq. Av. Adolfo Ruiz Cortines
										<br> Col. Obrero Campesina,
										<br> 91020
										<br> Xalapa-Enríquez, Ver.</p>
								</div>-->
						</div>
					</div>
					<!--/ end flickr -->

				</div><!-- Row end -->
			</div><!-- Container end -->
		</footer><!-- Footer end -->


		<!-- Copyright start -->
		<section id="copyright" class="copyright angle">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="footer-social unstyled">
							<li>
								<a target="_blank" title="Twitter" href="https://twitter.com/hightekoficial">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
								</a>
								<a target="_blank" title="Facebook"
									href="https://www.facebook.com/hightekoficial/?ref=settings">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
								</a>
								<a target="_blank" title="linkedin"
									href="https://www.linkedin.com/company/hightek-desarrollo-y-consultoría/">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
								</a>
								<a target="_blank" title="Instagram" href="https://www.instagram.com/hightekxalapa/">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-instagram"></i></span>
								</a>
								<a target="_blank" href="https://api.whatsapp.com/send?phone=522282836940">
									<span class="icon-pentagon wow bounceIn"><i class="fa fa-whatsapp"></i></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!--/ Row end -->
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="copyright-info">
							&copy; Hightek<a href="<?php echo get_site_url(); ?>/terms"> Términos y condiciones</a></span>
						</div>
					</div>
				</div>
				<!--/ Row end -->
				<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
					<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
				</div>
			</div>
			<!--/ Container end -->
		</section>
		<!--/ Copyright end -->

<?php get_footer(); ?>
