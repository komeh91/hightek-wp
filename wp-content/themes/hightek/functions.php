<?php
	$themename = "hightek";
	// Cargar opciones de temas en veracruz y en todo el multisite
	//require_once ( get_stylesheet_directory() . '/theme-options/theme-options.php' );

   
    wp_deregister_script( 'jquery' );

	// Habilita en el tema el soporte para los feed y rss
	add_theme_support( 'automatic-feed-links' );

	// Menu principal - Main menu
	register_nav_menus( array(
		'principal'   => __( 'Menu header y footer', $themename ),
	) );

	//iframe shortcode
	add_filter('widget_text', 'do_shortcode');

	add_editor_style();

    // Habilita las imagenes destacadas
	add_theme_support( 'post-thumbnails' );

    
    add_shortcode('iframe', 'iframe');
    function iframe($atts) {
        extract(shortcode_atts(array(
            'src' => "",
            'width' => "800",
            'height' => "600",
            'scrolling' => "no"
        ), $atts));

       $iframe = '<iframe src="'.$src.'" width="'.$width.'" height="'.$height.'" scrolling="'.$scrolling.'" allowtransparency="yes" frameborder="0" ></iframe>';

       return $iframe;
    }

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form'
	) );

	add_filter( 'wp_enqueue_scripts', 'desregister_jquery', PHP_INT_MAX );
	function desregister_jquery( ){
		wp_dequeue_script( 'jquery');
		wp_deregister_script( 'jquery' );
	}

    //walker menu
    class WPSE_Sublevel_Walker extends Walker_Nav_Menu{
        function start_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<div class='sub-menu-wrap'><ul class='sub-menu'>\n";
        }
        function end_lvl( &$output, $depth = 0, $args = array() ) {
            $indent = str_repeat("\t", $depth);
            $output .= "$indent</ul></div>\n";
        }
    }

function theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'theme' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar that appears on the left.', 'theme' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'theme_widgets_init' );
/*
function my_login_logo() {
    $blog_id = get_current_blog_id();
    switch_to_blog($id_blog);
        $general_settings = get_option("themeoption_general_setting");
    restore_current_blog();
?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo $general_settings['logo-principal']; ?>);
            background-size: contain;
            padding-bottom: 5px;
			width: 100%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
*/
function change_footer_admin() {
    echo 'hightek.com.mx';
}
add_filter('admin_footer_text', 'change_footer_admin');