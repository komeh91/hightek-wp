<?php get_header(); ?>
<script src="<?php bloginfo('template_url')?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/popper.min.js"></script>
<script src="<?php bloginfo('template_url')?>/js/bootstrap.min.js"></script>
<link href="<?php bloginfo('template_url')?>/style.css" rel="stylesheet">

<script type="text/javascript">
	/*load more ajax*/
    jQuery(document).ready( function($) {
        var loading = false;
        
        function setPagination( paramsHolder ) {
            var max             = paramsHolder.data( 'page-max' ),
                pageNum         = paramsHolder.data( 'page-start' ) + 1,
                nextLink        = paramsHolder.data( 'page-next' ),
                item            = paramsHolder.data( 'page-item' );
                loader          = $( '.posts-loader' );
            
            $( window ).scroll( function() {
                if ( ( ( $( this ).scrollTop() + $( this ).height() ) >= ( paramsHolder.height() + paramsHolder.offset().top ) ) && !loading ) {
                    loading = true;
                    loader.animate({
                        opacity : 1
                    });
                    
                    if ( pageNum <= max ) {
                        $.get( nextLink, function( data ) {
                            loader.animate({
                                opacity : 0
                            });
							
                            var link_next = $(location).attr('href');
                            var link_next = link_next.split("?s=");
                            var link_search = $(location).attr('search');
							nextLink = link_next[0] + 'page/'+ pageNum+"/?s="+link_next[1];
							console.log(nextLink);
														
							paramsHolder.attr( 'data-page-start', pageNum );
                            paramsHolder.attr( 'data-page-next', nextLink );
                            
                            $( data ).find( item ).appendTo( paramsHolder );
                            
                            loading = false;
							
                            if ( pageNum >= max ) {
                                loader.empty();
                                loader.html('No hay más coincidencias...');
                            }
                            pageNum++;

                        }).fail(function(error){
                            loader.empty();
                            loader.html('No hay más coincidencias...');
                        });

                    } else {
                        $( window ).unbind( 'scroll' );
                    }
					
                }
				return false;
            });
        }
        setPagination( $( '#content-nota' ) );
    });
</script>
<!--Si hay resultados-->
<div id="banner-area">
            <img src="<?php bloginfo('template_url'); ?>/images/banner/banner1.jpg" style="width: 100%;" alt="" />
            <div class="parallax-overlay"></div>
            <!-- Subpage title start -->
            <div class="banner-title-content">
                <div class="text-center">
                    <h2>Soluciones Tecnológicas</h2>
                    
                </div>
            </div><!-- Subpage title end -->
        </div><!-- Banner area end -->
<div class="container mrg-top-30">
    <?php 
    if(is_search()){
        global $wp_query;
        $total_results = $wp_query->found_posts;
        if( $total_results <= 0){
            $total_results = "0";
        ?>
            <div class="cant-result">
                <h3>No se han encontrado resultados para <span class="search-criteria">"<?php echo $_GET['s']; ?>"</span></h3>
                <?php get_search_form(); ?>
            </div>
        <?php 
        }else{ ?>
            <div class="cant-result">
                <h3>
                    Se han encontrado <?php echo $total_results; ?> resultados para 
                    <span class="search-criteria">
                       "<?php echo $_GET['s']; ?>"
                    </span>
                </h3>
            </div>
        <?php 
        }
        ?>
    <?php } ?>

    <div id="content-list">
            <?php 
            $cont = 1;
            $bus_cont = 1;
            $cont_break = 0;
            wp_reset_query();
            $paged          = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

            $args = array(
              's'           		=> $s,
              'posts_per_page'		=> 6, 
              'paged'               => $paged, 
            );
            $my_query   = new WP_Query( $args );
            $num_items = $my_query->post_count; 

            if ( $my_query->have_posts() ) :  ?>
            <div id="content-nota" class="row pagination-params list-notas-category list-notas-search" 
            data-page-max="<?php echo $my_query->max_num_pages; ?>" 
            data-page-start="<?php echo ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1; ?>" 
            data-page-next="<?php echo next_posts( $my_query->max_num_pages, false ); ?>" 
            data-page-item=".item-nota" data-num-items="<?php echo $num_items; ?>">
                    <?php 
                    $blog_id = get_current_blog_id();
                    while ( $my_query->have_posts()) {
                    $my_query->the_post(); 

                    $url = wp_get_attachment_image_src( get_post_thumbnail_id( $my_query->post->ID ), 'noticia-img' );
                    $imagen_destacada =  $url[0];
                    ?>
                    <div class="col-sm-6 col-md-4 col-lg-4 item-nota">
                        <div class="contenedorNota">
                           <?php 
                            if(empty($imagen_destacada)){
                               $imagen_destacada = get_bloginfo('template_url')."/images/default-image_0.png";
                            }
                           ?>
                            <a href="<?php the_permalink() ?>" class="overlay-hover-filtro hover-filtro-nota">
                                <div class='overlay-nota'></div>
                                <div class='overlay-filtro'></div>
                                <img class="img img-responsive img-change img-full" src="<?php echo $imagen_destacada; ?>" alt="<?php the_title();?>">
                            </a>
                            <div class="clearfix"></div>
                            <div class="titulo-nota">
                                <span>Fecha: <?php the_time( 'F j, Y ' ); ?></span>
                                <h5><?php the_title(); ?></h5>
                                <a href="<?php the_permalink(); ?>" class="btn-readmore">
                                    Continuar leyendo
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php 
                }
            ?>
            </div><!--content-nota-->
        <?php  endif; 
        wp_reset_postdata(); ?>
    </div> <!--#content-list-->
    <p class="posts-loader"><img src="<?php bloginfo( 'template_url' ); ?>/images/loading.gif" /></p>
</div><!--.container-->

<div class="gap-40"></div>
        <!-- Footer start -->
        <footer id="footer" class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 footer-widget">
                        <h3 class="widget-title">Proyectos Recientes</h3>
                        <div class="latest-post-items media">
                            <div class="latest-post-content media-body">
                                <h4><a target="_blank" href="https://medicalfit.com.mx/">MedicalFit</a></h4>
                                <p class="post-meta">
                                    <span class="author">El software que todos los nutriólogos aman.</span>
                                </p>
                            </div>
                        </div>
                        <div class="img-gallery">
                            <div class="img-container">
                                <a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/1.png">
                                    <img src="<?php bloginfo('template_url'); ?>/images/gallery/1.png" alt="">
                                </a>
                                <a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/2.png">
                                    <img src="<?php bloginfo('template_url'); ?>/images/gallery/2.png" alt="">
                                </a>
                                <a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/3.png">
                                    <img src="<?php bloginfo('template_url'); ?>/images/gallery/3.png" alt="">
                                </a>
                                <a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/4.png">
                                    <img src="<?php bloginfo('template_url'); ?>/images/gallery/4.png" alt="">
                                </a>
                                <a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/5.png">
                                    <img src="<?php bloginfo('template_url'); ?>/images/gallery/5.png" alt="">
                                </a>
                                <a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
                                    <img src="<?php bloginfo('template_url'); ?>/images/gallery/6.png" alt="">
                                </a>
                                <a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/6.png">
                                    <img src="<?php bloginfo('template_url'); ?>/images/gallery/7.png" alt="">
                                </a>
                                <a class="thumb-holder" data-rel="prettyPhoto" href="<?php bloginfo('template_url'); ?>/images/gallery/8.png">
                                    <img src="<?php bloginfo('template_url'); ?>/images/gallery/8.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--/ End Recent Posts-->


                    <div class="col-md-8 col-sm-12 footer-widget">
                        <h3 style=" color: white; ">Datos de contacto:</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Email:</h4>
                                <a href="mailto:ventas@hightek.com.mx" target="_top">ventas@hightek.com.mx</a>
                            </div>
                            <div class="col-md-6">
                                <h4>Teléfonos</h4>
                                <a href="tel:228290 1754" target="_top">+52 (228) 290 1754</a><br>
                                <a href="tel:228283 6940" target="_top">+52  (228) 283 6940</a><br>
 
                            </div>
                        </div>
                        <div class="row">
                            <div class="map col-md-8" id="map_canvas" data-latitude="" data-longitude=""
                                data-marker="<?php bloginfo('template_url'); ?>/images/marker.png">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d939.9997612532665!2d-96.93439361188314!3d19.541654563027674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85db3201d8d80f3d%3A0xf96cea85b29f989f!2sHightek%20Desarrollo%20y%20Consultor%C3%ADa!5e0!3m2!1ses!2smx!4v1575509694125!5m2!1ses!2smx"
                                    width="550" height="400" frameborder="0" style="border:0;"
                                    allowfullscreen=""></iframe>
                            </div>
                            <!--<div class="col-md-4">
                                        <h4 style="margin-top: 30px;" > Dirección:</h4>
                                        <p>Fraternidad No.12 (3er Piso)
                                            <br> Esq. Av. Adolfo Ruiz Cortines
                                            <br> Col. Obrero Campesina,
                                            <br> 91020
                                            <br> Xalapa-Enríquez, Ver.</p>
                                    </div>-->
                        </div>
                    </div>
                    <!--/ end flickr -->

                </div><!-- Row end -->
            </div><!-- Container end -->
        </footer><!-- Footer end -->



        <!-- Copyright start -->
        <section id="copyright" class="copyright angle">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="footer-social unstyled">
                            <li>
                                <a title="Twitter" href="https://twitter.com/hightekoficial">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
                                </a>
                                <a title="Facebook" href="https://www.facebook.com/hightekoficial/?ref=settings">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
                                </a>
                                <a title="linkedin"
                                    href="https://www.linkedin.com/company/hightek-desarrollo-y-consultoría/">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
                                </a>
                                <a title="Instagram" href="https://www.instagram.com/hightekxalapa/">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-instagram"></i></span>
                                </a>
                                <a target="_blank" href="https://api.whatsapp.com/send?phone=522282836940">
                                    <span class="icon-pentagon wow bounceIn"><i class="fa fa-whatsapp"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--/ Row end -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="copyright-info">
                            &copy; Hightek<a href="<?php echo get_site_url(); ?>/terms"> Términos y condiciones</a></span>
                        </div>
                    </div>
                </div>
                <!--/ Row end -->
                <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
                    <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
                </div>
            </div>
            <!--/ Container end -->
        </section>
        <!--/ Copyright end -->
<?php get_footer(); ?>
