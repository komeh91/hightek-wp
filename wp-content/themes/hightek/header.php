<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <title><?php echo is_front_page() ? get_bloginfo('name') : wp_title('') ." | ". get_bloginfo('description'); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <meta content="<?php bloginfo( 'html_type' ) ?>; charset=<?php bloginfo( 'charset' ) ?>" http-equiv="Content-Type" />
    <meta name="description" content="Hightek">
    <meta name="keywords" content="hightek">
    <meta name="author" content="Hightek">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!-- Apple -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- FB / 560x560 -->
 	<meta property="og:title" content="<?php bloginfo('name') ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="<?php bloginfo('template_url'); ?>/images/img-fb-meta-200.jpg" />
    <meta property="og:url" content="<?php  bloginfo('url'); ?>" />
    <meta property="og:description" content="Hightek" />
    <meta property="fb:admins" content="100000759628852" />

    <!-- Twitter / 560x300 -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@HightekOficial" />
    <meta name="twitter:creator" content="@HightekOficial" />
    <meta property="twitter:url" content="<?php  bloginfo('url'); ?>" />
	<meta property="twitter:title" content="<?php bloginfo('name') ?>" />
    <meta property="twitter:description" content="Hightek" />

    <?php //wp_head(); ?>

    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->

    <script async src="https://www.googletagmanager.com/gtag/js?id=G-DS78VDBZ2N"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() { dataLayer.push(arguments); }
		gtag('js', new Date());

		gtag('config', 'G-DS78VDBZ2N');


	</script>
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '190906912013960');
		fbq('track', 'PageView');
	</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=190906912013960&ev=PageView&noscript=1"
/></noscript>
	<link rel="icon" href="<?php bloginfo('template_url'); ?>/images/ic_hightek.ico">
	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugins/bootstrap/bootstrap.min.css">
	<!-- FontAwesome -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugins/fontawesome/font-awesome.min.css">
	<!-- Animation -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugins/animate.css">
	<!-- Prettyphoto -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugins/prettyPhoto.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugins/owl/owl.carousel.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugins/owl/owl.theme.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugins/flex-slider/flexslider.css">
	<!-- Flexslider -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/plugins/cd-hero/cd-hero.css">
	<!-- Style -->
	<link id="style-switch" href="<?php bloginfo('template_url'); ?>/css/colors-palette.css" media="screen" rel="stylesheet" type="text/css">
	<!-- Main Stylesheet -->
	<link href="<?php bloginfo('template_url'); ?>/css/style.css" rel="stylesheet">
</head>
<body style="font-size: 1em; zoom: 1;">
	<div class="body-inner">
		<!-- Header start -->
		<header id="header" class="fixed-top header" role="banner">
			<div class="container">
				<nav class="navbar navbar-expand-lg navbar-dark">
					<a class="navbar-brand img-style navbar-bg " href="index.html" style="padding-top: 1%; "><img
							class="img-resposive img-fluid float-right img-responsives img-responsives"
							src="<?php bloginfo('template_url'); ?>/images/hightek380.png" style="max-width:100px" style="max-width:100px" alt="logo"></a>
					<button class="navbar-toggler ml-auto border-0 rounded-0 text-white" type="button"
						data-toggle="collapse" data-target="#navigation" aria-controls="navigation"
						aria-expanded="false" aria-label="Toggle navigation">
						<span class="fa fa-bars"></span>
					</button>

					<div class="collapse navbar-collapse text-center" id="navigation">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item dropdown active">
								<a class="nav-link" href="<?php echo get_site_url(); ?>" role="button">
									Inicio
								</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">
									Compañía
								</a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/conocenos">Conócenos</a>
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/bolsa-de-trabajo">Bolsa de trabajo</a>
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/terms">Términos y condiciones</a>

									<!--<a class="dropdown-item" href="testimonial.html">Testimonios</a>-->
								</div>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="false">
									Servicios
								</a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/software">Software</a>
 									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/app">Aplicaciones móviles</a>
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/ecommerce">Ecommerce</a>
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/redes-sociales">Marketing y redes sociales</a>
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/consultoria">Consultoría</a>
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/capacitación">Capacitación</a>
									<a class="dropdown-item" href="<?php echo get_site_url(); ?>/sitios-we">Páginas WEB</a>
 								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo get_site_url(); ?>/contacto">Contacto</a></a>
							</li>
							<?php
							    // Get the ID of a given category
							    $category_id = get_cat_ID( 'blog' );
							 
							    // Get the URL of this category
							    $category_link = get_category_link( $category_id );
							?>
							<li class="nav-item">
								<a class="nav-link" href="<?php echo $category_link; ?>">Blog</a></a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
		<!--/ Header end -->
