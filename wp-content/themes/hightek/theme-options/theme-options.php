<?php
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	$menu = add_menu_page( __( 'Opciones de tema', 'sampletheme' ), __( 'Opciones generales', 'sampletheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
	add_action( 'admin_print_styles-' . $menu, 'admin_theme_style' );
	add_action( 'admin_print_scripts-' . $menu, 'admin_theme_script' );
}
function admin_theme_style(){
	wp_enqueue_style( 'style_theme_option', get_template_directory_uri().'/theme-options/style.css');
}
function admin_theme_script(){
	wp_enqueue_script( 'script_theme_boostrap', get_template_directory_uri().'/js/bootstrap.min.js');
	wp_enqueue_media();
	wp_enqueue_script( 'editor' );
	
	wp_register_script( 'script_theme_functions', get_template_directory_uri().'/theme-options/js/functions.js' );
	wp_enqueue_script( 'script_theme_functions' );
	
	$translation_array = array( 
        'templateUrl' => get_stylesheet_directory_uri(),
        'blog_id'     => get_current_blog_id()
    );
	//after wp_enqueue_script
	wp_localize_script( 'script_theme_functions', 'theme_metadata', $translation_array );
}

function theme_options_do_page() {
	?>
	<div class="wrap">
		<div class="col-lg-12">
			<?php echo "<h2 class='title-theme-option'>" . wp_get_theme() . __( ' (Opciones del tema)', 'sampletheme' ) . "</h2>"; ?>
        </div>
		<form class="theme_options" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="theme-options-panel">
			<?php get_template_part( 'theme-options/content', 'template' ); ?> 
        </form>
	</div>
	<?php
}

function themeoption_save(){
	$general_settings = array(
		"logo-principal"		 => $_POST["logo-principal"],
		"text-header-title"      => $_POST["text-header-title"],
		"footter-description"  	 => $_POST["footter-description"],
		"code-google-analytics"  => $_POST["code-google-analytics"],
		"category-slider"  		 => $_POST["category-slider"],
		"category-noticias"  	 => $_POST["category-noticias"],
        "twitter-timeline"  	 => $_POST["twitter-timeline"],
		"facebook-url"     		 => $_POST["facebook-url"],
		"twitter-url"     		 => $_POST["twitter-url"],
		"youtube-url"     	 	 => $_POST["youtube-url"],
		"e-mail"     		 	 => $_POST["e-mail"],
	);
	$sidebar_options = array(
		"sidebar-active"		 => $_POST["sidebar-active"],
	);
	
	$banners_sidebar = array(
		"image_file" => array_filter($_POST["image_file"], 'removeEmptyElements'), 
		"image_url"  => array_filter($_POST["image_url"], 'removeEmptyElements'),
		"image_global"  => array_filter($_POST["image_global"], 'removeEmptyElements'),
        "new_window" => array_filter($_POST['new_window'], 'removeEmptyElements')
	);
	
	$veda_electoral = array(
		"veda_electoral" => $_POST["veda_electoral"],
	);
	
	echo json_encode($_POST);
	update_option("themeoption_general_setting", array_merge($general_settings));
	update_option("themeoption_sidebar_options", array_merge($sidebar_options));
	update_option("themeoption_banners_sidebar", $banners_sidebar);
	update_option("themeoption_veda_electoral", $veda_electoral);
	exit();
}
add_action('wp_ajax_themeoption_save', 'themeoption_save');

/* Ajax banners multisite - p*/
function themeoption_sites_save(){
	$banner = $_POST['banner'];
	
	$global_banner = array();
	
	$global_banner['image_file'] = $banner[0];
	$global_banner['image_url'] = $banner[1];
	
	switch_to_blog( 2 );
		$banners_sidebar  = get_option("themeoption_banners_sidebar");
		$array_resuly = array_merge($global_banner, $banners_sidebar );
		update_option("themeoption_banners_sidebar", $array_resuly);
	restore_current_blog();
	die();
}
add_action('wp_ajax_themeoption_sites_save', 'themeoption_sites_save');


function theme_after_setup_theme(){
	if(!get_option($themename . "_installed")){		
		
		$general_settings = array(
			"logo-url-main" => $_POST["logo-url-main"],
		);
		$slider_option = array(
			"slider1" => "Inspiration",
		);
		
		add_option($themename . "_general_setting", $general_settings);
		add_option($themename . "_installed", 1);
	}
}
add_action("after_setup_theme", "theme_after_setup_theme");

function theme_switch_theme($theme_template){
	global $themename;
	delete_option($themename . "_installed");
}
add_action("switch_theme", "theme_switch_theme");