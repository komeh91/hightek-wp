<!-- Nav tabs -->
<div class="col-md-3">
	<div class="content-tab-link">
        <ul class="nav nav-tabs" role="tablist">
          <li><a class="active" href="#general-settings" role="tab" data-toggle="tab">Opciones generales</a></li>  
        </ul>
    </div>
</div>
<!-- Tab panes -->
<div class="col-md-9">
	<?php 
	if($_POST["action"]=="themeoption_save"){
	?>
	<div class="updated"> 
		<p>
			<strong>
				<?php _e('Opciones guardadas', 'Veracruz2017'); ?>
			</strong>
		</p>
	</div>
	<?php } ?>
 	
    <div class="tab-content">
        <div class="tab-pane active form-horizontal" id="general-settings">
			<?php get_template_part( 'theme-options/template/general', 'setting' ); ?> 
        </div> <!--#general-settings-->
      	<div class="tab-pane form-horizontal" id="banner-sidebar">
        	<?php get_template_part( 'theme-options/template/sidebar', 'options' ); ?> 
        </div> <!--#banner-sidebar-->
        <div class="tab-pane form-horizontal" id="veda-electoral">
        	<?php get_template_part( 'theme-options/template/veda', 'electoral' ); ?> 
        </div> <!--#veda-electoral-->
        <div class="clearfix"></div>
        <img id="theme_options_preloader" src="<?php echo get_template_directory_uri();?>/theme-options/images/ajax-loader.gif" />
        <img id="theme_options_tick" src="<?php echo get_template_directory_uri();?>/theme-options/images/tick.png" />
        <div class="pull-right">
            <input type="hidden" name="action" value="themeoption_save" />
            <input type="submit" class="btn-custom" name="submit" value="Guardar opciones" />
        </div>
         
    </div>
</div>
