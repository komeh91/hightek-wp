// JavaScript Document
var Theme_init = {
	templateUrl: theme_metadata.templateUrl,
    blog_id: theme_metadata.blog_id,
	init: function(){
		Theme_init.saveThemeOption()
                  .exec_dragdrop()
                  .addMoreFieldOvh()
				  .addMoreFieldHome();
	},
	addMedia: function(click_media){
		var button = jQuery("#"+click_media);
		var send_attachment = wp.media.editor.send.attachment;
		wp.media.editor.send.attachment = function(props, attachment){
			button.html("<img src='"+attachment.url+"' width='120px'>");
			jQuery("input[name*='"+click_media+"']").val(attachment.url);
			
			if(attachment.url != ''){
				jQuery( '.remove-p-'+click_media).css('display', 'block');
			}else{
				jQuery( '.remove-p-'+click_media).css('display', 'none');
			}
			wp.media.editor.send.attachment = send_attachment;
		}
		wp.media.editor.open(button);
		return false;
		return this;
	},
	removeMedia: function(click_media){
		var img_content =   "<img src='"+Theme_init.templateUrl+"/theme-options/images/img-not-found.png' width='60px'>";
			img_content +=  "<p> Asignar logo</p>";
		var button = jQuery("#"+click_media);
		jQuery(document).on('click', '#remove-'+click_media , function(event) {
			event.preventDefault();
			jQuery( "input[name*='"+click_media+"']" ).val('');
			button.html(img_content);
			jQuery( '.remove-p-'+click_media).css('display', 'none');
		});
		return this;
	},
    exec_dragdrop: function(){
        jQuery( "#content-form" ).sortable({
          placeholder: "ui-state-highlight"
        });
        jQuery( "#content-form" ).disableSelection();
        return this;
	},
	addMoreFieldHome: function(){
		var addmoreitem = jQuery(document);
		var deleteitem =  jQuery(document);
		var uploadButton =  jQuery(document);

		uploadButton.on('click','.btn-upload', function(){
			var button = jQuery(this);
			var index = jQuery(this).data('index');

			var send_attachment = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment){
				jQuery( '#image_file_'+ index ).val(attachment.url);
				jQuery( '.img-preview-'+ index ).attr('src', attachment.url);
				wp.media.editor.send.attachment = send_attachment;
			}
			wp.media.editor.open(button);
			return false;
		});

		addmoreitem.on('click', '.btn-add-more-home', function(){
			var num_sliders = jQuery("#content-form .item-form-rich").length;
			jQuery("#add-more-home").remove();
			var form_slider =   "<div class='item-form-rich'>";
					form_slider +=	"<a class='delete-item'>";
					form_slider +=		"<img src='"+Theme_init.templateUrl+"/theme-options/images/btn-close.png'>";
					form_slider +=  "</a>";
					form_slider +=	"<div class='col-md-10'>";
					form_slider +=		"<div class='form-group'>";
					form_slider +=			"<label for='content' class='col-md-2 control-label'>Imagen</label>";
					form_slider +=			"<div class='col-md-10'>";
					form_slider +=				"<div class='col-md-10 sin-padding-left'>";
					form_slider +=					"<input type='text' class='form-control form-upload input-sm' id='image_file_"+num_sliders+"' name='image_file[]'>";
					form_slider +=				"</div>";
					form_slider +=				"<div class='col-md-2 sin-padding'>";
					form_slider +=					"<input type='button' class='form-control button-primary form-btn-upload btn-upload btn-style' value='Subir' id='uploadimage"+num_sliders+"' data-index='"+num_sliders+"'/>";
					form_slider +=				"</div>";
					form_slider +=			"</div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='form-group'>";
					form_slider += 			"<label for='content' class='col-md-2 control-label'>Url</label>";
					form_slider +=			"<div class='col-md-10'>";
					form_slider +=				"<input type='text' name='image_url[]' id='image_url_"+num_sliders+"' class='form-control input-sm regular-text'/>";
					form_slider +=			"</div>";
					form_slider +=			"<div class='clearfix'></div>";
					form_slider +=		"</div>";
                    
                    if( Theme_init.blog_id == 1 ){
                        form_slider +=  "<div class='form-grupo row'>";
                        form_slider +=      "<div class='col-md-6 row'>";
                        form_slider +=          "<label for='content' class='col-md-4 control-label'>Global</label>";
                        form_slider +=          "<div class='col-md-8'>";
                        form_slider +=              "<label class='switch'>";
                        form_slider +=                  "<input type='hidden' id='image_global_"+num_sliders+"' name='image_global[]' value='0'><input type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value'>";																										 
                        form_slider +=                  "<div class='slider round'></div>";
                        form_slider +=              "</label>";
                        form_slider +=          "</div>";
                        form_slider +=      "</div>";
                        form_slider +=      "<div class='col-md-6 row'>";
                        form_slider +=          "<label for='content' class='col-md-6 control-label'>Nueva ventana</label>";
                        form_slider +=          "<label class='switch'>";
                        form_slider +=              "<input type='hidden' id='new_window_"+num_sliders+"' name='new_window[]' value='0'><input type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value'>";
                        form_slider +=              "<div class='slider round'></div>";
                        form_slider +=          "</label>";
                        form_slider +=      "</div>";
                        form_slider +=      "<div class='clearfix'></div>";
                        form_slider +=  "</div>";      
                    }else{
                        form_slider +=      "<div class='form-group'>";
                        form_slider +=          "<label for='content' class='col-md-2 control-label'>Nueva ventana</label>";
                        form_slider +=          "<label class='switch ctm-checkbox'>";
                        form_slider +=              "<input type='hidden' id='new_window_"+num_sliders+"' name='new_window[]' value='0'><input type='checkbox' onclick='this.previousSibling.value=1-this.previousSibling.value'>";
                        form_slider +=              "<div class='slider round'></div>";
                        form_slider +=          "</label>";
                        form_slider +=      "</div>";     
                    }
                    form_slider +=		"<div class='clearfix'></div>";
					form_slider +=	"</div>";
					form_slider +=  "<div class='col-md-2'>";
                    form_slider +=  "<img class='img-full img-preview-"+num_sliders+"' src='"+Theme_init.templateUrl+"/theme-options/images/img-not-found.png'>";
        			form_slider +=  "</div>";
					form_slider +=	"<div class='clearfix'></div>";
				form_slider +=  "</div>";
				form_slider += "<input type='button' id='add-more-home' class='btn btn-default btn-add-more-home' value='Agregar más'>";
				jQuery("#content-form").append(form_slider);
		});
		deleteitem.on('click', '.delete-item' ,function() {
			jQuery(this).parent().remove();
		});
		return this;
	},
	saveThemeOption: function(){
		jQuery("#theme-options-panel").submit(function(event){
			event.preventDefault();
			var self = jQuery(this);
			var data = self.serializeArray();
			jQuery("#theme_options_preloader").css("display", "block");
			jQuery("#theme_options_tick").css("display", "none");
			jQuery.ajax({
				url: ajaxurl,
				type: 'post',
				dataType: 'json',
				data: data,
				success: function(json){
					/*
					var array_banner = Array();
						array_banner.push(json['image_file']);
						array_banner.push(json['image_url']);
						Theme_init.saveThemeOptionSave(array_banner);	
					*/

					jQuery("#theme_options_preloader").css("display", "none");
					jQuery("#theme_options_tick").css("display", "block");
				}
			});
		});
		return this;
	},
    addMoreFieldOvh: function(){
		var addmoreitem = jQuery(document);
		var deleteitem =  jQuery(document);
		var uploadButton =  jQuery(document);

		uploadButton.on('click','.btn-upload', function(){
			var button = jQuery(this);
			var index = jQuery(this).data('index');

			var send_attachment = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment){
				jQuery( '#image_file_'+ index ).val(attachment.url);
				jQuery( '.img-preview-'+ index ).attr('src', attachment.url);
				wp.media.editor.send.attachment = send_attachment;
			}
			wp.media.editor.open(button);
			return false;
		});

		addmoreitem.on('click', '.btn-add-more-home-ovh', function(){
			var num_sliders = jQuery("#content-form-ovh .item-form-rich").length;
			jQuery("#add-more-home-ovh").remove();
			var form_slider =   "<div class='item-form-rich'>";
					form_slider +=	"<a class='delete-item'>";
					form_slider +=		"<img src='"+Theme_init.templateUrl+"/theme-options/images/btn-close.png'>";
					form_slider +=  "</a>";
					form_slider +=	"<div class='col-md-10'>";
					form_slider +=		"<div class='form-group'>";
					form_slider +=			"<label for='content' class='col-md-2 control-label'>Imagen</label>";
					form_slider +=			"<div class='col-md-10'>";
					form_slider +=				"<div class='col-md-10 sin-padding-left'>";
					form_slider +=					"<input type='text' class='form-control form-upload input-sm' id='image_file_"+num_sliders+"' name='image_file[]'>";
					form_slider +=				"</div>";
					form_slider +=				"<div class='col-md-2 sin-padding'>";
					form_slider +=					"<input type='button' class='form-control button-primary form-btn-upload btn-upload btn-style' value='Subir' id='uploadimage"+num_sliders+"' data-index='"+num_sliders+"'/>";
					form_slider +=				"</div>";
					form_slider +=			"</div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='form-group'>";
					form_slider += 			"<label for='content' class='col-md-2 control-label'>Url</label>";
					form_slider +=			"<div class='col-md-10'>";
					form_slider +=				"<input type='text' name='image_url[]' id='image_url_"+num_sliders+"' class='form-control input-sm regular-text'/>";
					form_slider +=			"</div>";
					form_slider +=			"<div class='clearfix'></div>";
					form_slider +=		"</div>";
					form_slider +=		"<div class='clearfix'></div>";
					form_slider +=	"</div>";
					form_slider +=  "<div class='col-md-2'>";
            			form_slider +=  "<img class='img-full img-preview-"+num_sliders+"' src='"+Theme_init.templateUrl+"/theme-options/images/img-not-found.png'>";
        			form_slider +=  "</div>";
					form_slider +=	"<div class='clearfix'></div>";
				form_slider +=  "</div>";
				form_slider += "<input type='button' id='add-more-home-ovh' class='btn btn-default btn-add-more-home-ovh' value='Agregar más'>";
				jQuery("#content-form-ovh").append(form_slider);
		});
		deleteitem.on('click', '.delete-item' ,function() {
			jQuery(this).parent().remove();
		});
		return this;
	},
	saveThemeOptionSave: function(data){
		jQuery.ajax({
			url: ajaxurl,
			type: 'post',
			dataType: 'json',
			data: {'action' : 'themeoption_sites_save', 'banner' : data },
			success: function(json){
				console.log(json);
			}
		});
	}
};
jQuery(document).on('ready', Theme_init.init);



/*
jQuery(document).ready(function($) {
	$(document).on('ready',function() {
		var templateUrl = theme_name.templateUrl;
		console.log(templateUrl);
		$("#theme-options-panel").submit(function(event){
			event.preventDefault();
			var self = $(this);
			var data = self.serializeArray();
			$("#theme_options_preloader").css("display", "block");
			$("#theme_options_tick").css("display", "none");
			$.ajax({
					url: ajaxurl,
					type: 'post',
					dataType: 'json',
					data: data,
					success: function(json){
						console.log(data);
						$("#theme_options_preloader").css("display", "none");
						$("#theme_options_tick").css("display", "block");
					}
			});
		});
	
		$( '#uploadimg' ).on( 'click', function() {
			var button = $(this);
			var send_attachment = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment){
				$( '.logo-preview').attr('src',attachment.url);
				$( '#img-evento-1' ).val(attachment.url);
				wp.media.editor.send.attachment = send_attachment;
			}
			wp.media.editor.open(button);
			return false;
		});
		$( '#uploadimg2' ).on( 'click', function() {
			var button = $(this);
			var send_attachment = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment){
				$( '.logo-preview2').attr('src',attachment.url);
				$( '#img-evento-2' ).val(attachment.url);
				wp.media.editor.send.attachment = send_attachment;
			}
			wp.media.editor.open(button);
			return false;
		});
		
		//firts upload media slider
		$( '#uploadimage0' ).on( 'click', function() {
			var button = $(this);
			var send_attachment = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment){
				$( '.img-preview-0').attr('src',attachment.url);
				$( '#slide-img-0' ).val(attachment.url);
				wp.media.editor.send.attachment = send_attachment;
			}
			wp.media.editor.open(button);
			return false;
		});
		
		
		
		$(document).on('click','#add-more',function() {
			var num_sliders = $("#content-img-slider .item-slider").length;
			$("#add-more").remove();
			var form_slider =   "<div class='item-slider'>";
					form_slider +=  "<a class='delete-slide'>";
					form_slider += 		"<img src='"+templateUrl+"/theme-options/images/btn-close.png'>";
					form_slider += "</a>";
					form_slider += "<div class='col-md-2'>";
					form_slider += 		"<img class='img-preview-"+num_sliders+" img-full' src='"+templateUrl+"/theme-options/images/dummy-83x83.png'>";
					form_slider += "</div>";
					form_slider += "<div class='col-md-10'>";
					form_slider += 		"<div class='form-group'>";
					form_slider += 			"<div class='col-md-9 sin-padding-left'>";
					form_slider += 				"<input type='text' class='form-control input-sm' id='slide-img-"+num_sliders+"' name='slide-img[]' placeholder='Url Slider'>";
					form_slider += 			"</div>";
					form_slider += 			"<div class='col-md-3 sin-padding-righ'>";
					form_slider += 				"<input type='button' class='button-primary form-control' value='Subir imagen' id='uploadimage"+num_sliders+"'/>";
					form_slider += 			"</div>";
					form_slider += 			"<div class='clearfix'></div>"; 
					form_slider += 		"</div>";
					form_slider += 		"<div class='form-group'>";
					form_slider += 			"<input type='text' class='form-control input-sm' id='slide-url-"+num_sliders+"' name='slide-url[]' placeholder='Link de slider'>";
					form_slider += 		"</div>";
					form_slider += 		"<div class='form-group'>";
					form_slider += 			"<input type='text' class='form-control input-sm' id='slide-title-"+num_sliders+"' name='slide-title[]' placeholder='Titúlo de slider'>";
					form_slider += 		"</div>";
					form_slider += "</div>";
					form_slider += "<div class='clearfix'></div>"
				form_slider += "</div>";
				form_slider += "<input type='button' id='add-more' class='btn-add-more' value='Add More'>";
				
			$("#content-img-slider").append(form_slider);
			
			$(document).on('click', '#uploadimage'+num_sliders , function() {
				var button = $(this);
				var send_attachment = wp.media.editor.send.attachment;
				wp.media.editor.send.attachment = function(props, attachment){
					$( '.img-preview-'+num_sliders).attr('src',attachment.url);
					$( '#slide-img-'+num_sliders ).val(attachment.url);
					wp.media.editor.send.attachment = send_attachment;
				}
				wp.media.editor.open(button);
				return false;
			});	
		});
		
		
		$(document).on('click','.delete-slide', function() {
			$(this).parent().remove();
		});
		
		
		
		$( '#uploadlogo0' ).on( 'click', function() {
			var button = $(this);
			var send_attachment = wp.media.editor.send.attachment;
			wp.media.editor.send.attachment = function(props, attachment){
				$( '.logo-preview-0').attr('src',attachment.url);
				$( '#logo-img-0' ).val(attachment.url);
				wp.media.editor.send.attachment = send_attachment;
			}
			wp.media.editor.open(button);
			return false;
		});
		
		$(document).on('click','#add-more-logo',function() {
			var num_logos = $("#content-img-logo .item-logo").length;
			$("#add-more-logo").remove();
			var form_logo =   "<div class='item-logo'>";
					form_logo +=  "<a class='delete-logo'>";
					form_logo += 		"<img src='"+templateUrl+"/theme-options/images/btn-close.png'>";
					form_logo += "</a>";
					form_logo += "<div class='col-md-2'>";
					form_logo += 		"<img class='logo-preview-"+num_logos+" img-full' src='"+templateUrl+"/theme-options/images/dummy-83x83.png'>";
					form_logo += "</div>";
					form_logo += "<div class='col-md-10'>";
					form_logo += 		"<div class='form-group'>";
					form_logo += 			"<div class='col-md-9 sin-padding-left'>";
					form_logo += 				"<input type='text' class='form-control input-sm' id='logo-img-"+num_logos+"' name='logo-img[]' placeholder='Url logo'>";
					form_logo += 			"</div>";
					form_logo += 			"<div class='col-md-3 sin-padding-righ'>";
					form_logo += 				"<input type='button' class='button-primary form-control' value='Subir imagen' id='uploadlogo"+num_logos+"'/>";
					form_logo += 			"</div>";
					form_logo += 			"<div class='clearfix'></div>"; 
					form_logo += 		"</div>";
					form_logo += 		"<div class='form-group'>";
					form_logo += 			"<input type='text' class='form-control input-sm' id='logo-url-"+num_logos+"' name='logo-url[]' placeholder='Link de logo'>";
					form_logo += 		"</div>";
					form_logo += 		"<div class='form-group'>";
					form_logo += 			"<input type='text' class='form-control input-sm' id='logo-title-"+num_logos+"' name='logo-title[]' placeholder='Titúlo de logo'>";
					form_logo += 		"</div>";
					form_logo += "</div>";
					form_logo += "<div class='clearfix'></div>"
				form_logo += "</div>";
				form_logo += "<input type='button' id='add-more-logo' class='btn-add-more' value='Add More'>";
				
			$("#content-img-logo").append(form_logo);
			
			$(document).on('click', '#uploadlogo'+num_logos , function() {
				var button = $(this);
				var send_attachment = wp.media.editor.send.attachment;
				wp.media.editor.send.attachment = function(props, attachment){
					$( '.logo-preview-'+num_logos).attr('src',attachment.url);
					$( '#logo-img-'+num_logos ).val(attachment.url);
					wp.media.editor.send.attachment = send_attachment;
				}
				wp.media.editor.open(button);
				return false;
			});	
		});
		
		
		$(document).on('click','.delete-logo', function() {
			$(this).parent().remove();
		});
			
	});
});
*/	
