<?php 
	$veda_electoral = get_option("themeoption_veda_electoral");
?>
<div class="title-section">
	<h3>Activar Veda Electoral</h3>    
</div>

<div class="form-group">
    <div class="col-lg-3 label-option">
        <label for="text-header-title" class="control-label">Veda Electoral</label>
        <span class="description-title">
        	(Default : no)<br>
            Activar Veda Electoral en todos los sitios de la red.
        </span>
    </div>
    <div class="col-lg-9">
        <label class="switch top-switch">
            <input type="checkbox" name="veda_electoral" <?php echo ( !empty($veda_electoral['veda_electoral']) ) ? 'checked' : ''; ?>>
            <div class="slider round"></div>
        </label>
    </div>
</div>
