<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hightek_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>1Fnp-:TK7>D< a&j#/ Z8#k,[kiUzg|eulXYY(2*z5p4JFwF!.7gC8pV+O2XSwf' );
define( 'SECURE_AUTH_KEY',  '<]=DFY5>qZM1gN|2?>t/AJ~9~=@#k/e>$U!<fEw8bNJ3F0Xl2M(*OMp9L+#jNj5s' );
define( 'LOGGED_IN_KEY',    'Q%>`h>|!7}9QzYV(L=$@>[=:4!x_.<|[2jD0#mL1X;t;x|KsdVJ4SSbrWk_ F&^P' );
define( 'NONCE_KEY',        '2sg@~e1UE^2Y`Zv*OIOsKc;YP2?gw*>3nIU(0[&QF!$sc)Gv|%hAC64.uXec50]P' );
define( 'AUTH_SALT',        './V=0|VWs][^(k2*W;Dg9%,iL8>BiT[;% R&Ec@OhMty|7@w]I4wEJ)<M(~oKO@4' );
define( 'SECURE_AUTH_SALT', 'V*qARfv1gCp}JA8F?gq{t&Z~p/ZU;XAZHj:uRzEIhI^/_d80;<_& ;wg(/Ov<I^Q' );
define( 'LOGGED_IN_SALT',   'u(/QQg{x?ALSc=lXD))4}ffI_X[=nlF4u3{JI}.h)D~Z8jf,d/Uxi:A CtmO#HhG' );
define( 'NONCE_SALT',       '3XMV)d#p!}2)b4sI><y$h<XUqWFGLzJ!jK|ca3O1]6t0R+i5y4xu/I7povz_Mk:/' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
